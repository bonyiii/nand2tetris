// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

(KEYLOOP)
@rowcounter   // Define row counter
M=0  // Set its value equal to 0

@SCREEN  // Goto screen memory address
D=A      // Set the data register value equal to the address
@address // Define a new variable which will hold the screen row/coll address at 17
M=D      // sets the variable value equal to address of the screen initially, it will be incremented as we go

// Read keyboard
@KBD
D=M
@KEYPRESSED
D;JGT
@KEYNOTPRESSED
0;JMP

(ROWLOOP)
// Define number of columns
@colcounter // Define column counter variable
M=0         // set its initial value within the row iteration to 0


(COLLOOP)
// Color the column black
@key
D=M
@address
A=M
M=D

// incerement column counter, increment address 
@colcounter
M=M+1 // increment by 1
@1 // move to next word (eg, next column)
D=A  // set the value into data register
@address // go to address
M=D+M  // add address value and data register value

// Read column iterator value, substract it from 31
// if it is greater then zero jump to next column
@colcounter
D=M
@32  // goto address 31, iterate over 32 columns (0..31)
D=A-D   // substract counter value from 31
@COLLOOP
D;JGT // if counter > 0 goto COLLOOP


// Increment row counter
@rowcounter
M=M+1

// Read row iterator value, substract it from 15
@rowcounter
D=M
@256
D=A-D // if counter > 0 goto ROWLOOP
@ROWLOOP
D;JGT

@KEYLOOP
0;JMP


(KEYPRESSED)
@key
M=-1
@ROWLOOP
0;JMP

(KEYNOTPRESSED)
@key
M=0
@ROWLOOP
0;JMP


(END)
@END
0;JMP
