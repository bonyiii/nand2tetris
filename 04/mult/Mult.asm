// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)
//
// This program only needs to handle arguments that satisfy
// R0 >= 0, R1 >= 0, and R0*R1 < 32768.

// Put your code here.

// R0
// for i in R1
// R0 + R0

@R2
M=0  // sum = 0

(LOOP)
        @R0
        D=M // D = RAM[0]

        // Check if R1 (counter) is 0 if goto STOP
        @R1
        D=M
        @STOP
        D;JEQ


        // Add R0 to sum which is storead at R2
        @R0
        D=M
        @R2
        M=D+M

        // Decrement counter
        @R1
        M=M-1

        @LOOP
        0;JMP // Jump back to the beginning of the loop

(STOP)
        @R2

(END)
        @END
        0;JMP  // Infinite loop
