class Parser:
  C_ARITHMETIC = 'CA'
  C_PUSH = 'push'
  C_POP = 'pop'

  def __init__(self, lines):
    self.lines = lines
    self.current_command = None
    self.idx = 0
    self._strip_comments()

  def has_more_commands(self):
    return self.idx < len(self.lines)

  def advance(self):
    self.idx += 1
    self.current_command = self.current_line().split()

  def command_type(self):
    if self.command() in ['add', 'sub', 'neg', 'eq', 'gt', 'lt', 'and', 'or', 'not']:
      return self.C_ARITHMETIC
    elif self.command() == self.C_PUSH:
      return self.C_PUSH
    elif self.command() == self.C_POP:
      return self.C_POP
    else:
      return self.command()

  def current_line(self):
    return self.lines[self.idx-1]

  def arg1(self):
    if self._command_length() in [2, 3]:
      return self._arg1()

  def arg2(self):
    if self._command_length() == 3:
      return self._arg2()

  def command(self):
    return self.current_command[0]

  def _command_length(self):
    return len(self.current_command)

  def _arg1(self):
    return self.current_command[1]

  def _arg2(self):
    return int(self.current_command[2])

  def _strip_comments(self):
    code_lines = []
    for line in self.lines:
      code = line.strip().split('//')[0]
      if code:
        code_lines.append(code)
    self.lines = code_lines
