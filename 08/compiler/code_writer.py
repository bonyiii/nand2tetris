from parser import Parser
from modules.arithmetic import Arithmetic
from modules.branching import Branching
from modules.memory_segment import MemorySegment
from modules.function import Function

class CodeWriter:
  def __init__(self):
    self.lines = []
    self.label_idx = 0

    self.static_namespace = None
    self.current_function_name = None

  def set_file_name(self, static_namespace):
    self.static_namespace = static_namespace
    self.current_function_name = None

  def write(self, parser):
    self.current_function_name = self.current_function_name or self.static_namespace
    self.lines.append("// {}".format(parser.current_line()))

    #print(parser.current_command, parser.current_line())
    if parser.command_type() == Parser.C_PUSH:
      MemorySegment(stack = self).push(parser.arg1(), parser.arg2())
    elif parser.command_type() == Parser.C_POP:
      MemorySegment(stack = self).pop(parser.arg1(), parser.arg2())
    elif parser.command_type() == Parser.C_ARITHMETIC:
      Arithmetic(stack = self).run(parser.command())
    elif parser.command_type() in ['label', 'goto', 'if-goto']:
      Branching(stack = self).run(parser.command(), parser.arg1())
    elif parser.command_type() in ['call', 'function', 'return']:
      Function(stack = self).run(parser.command(), parser.arg1(), parser.arg2())

  def bootstrap(self):
    self.lines.append("//Bootstrap")
    self.lines.append("@256")
    self.lines.append("D=A")
    self.lines.append("@SP")
    self.lines.append("M=D")
    Function(stack = self).run("call", "Sys.init", 0)

  def push(self):
    # Go to stack pointer location and by that select RAM memory register
    self.lines.append("@SP")
    # Set address to the value stored at the RAM memory register, eg: *SP (the value where the stack pointer points to)
    self.lines.append("A=M")
    # Save CPU data register value into the RAM memory register
    self.lines.append("M=D")
    # Go to the stack pointer location
    self.lines.append("@SP")
    # Increment stack pointer value
    # the @SP location is fixed in the memory, its value points to the next available space
    # so that's why we update the register value
    self.lines.append("M=M+1")

  def pop(self):
    """
    Set A register to @SP value, so next time when M is read, it will return @SP value (eg: Go to memory segment where @SP points)

    Set A to value - 1, eg: jump to value-1, so next time M at that address will be read
    Set M to value - 1, eg: decrement SP value to point to this new address
    """

    # Go to the stack pointer location
    self.lines.append("@SP")
    # Decrement the stack pointer value (eg: the @SP register value gets decremented by 1),
    # and jumps to the (old value - 1) of the @SP register,
    self.lines.append("AM=M-1")

  def get(self):
    """
    Set A register to @SP so next time when M is read it will return the @SP value (Go to memory segment where @SP points)

    Set A to value - 1, eg: jump to that value, this way next time we read M value from that register will be read)
    Don't change SP, so next push will overwrite this M memory segment's value
    """

    # Go to the stack pointer location
    self.lines.append("@SP")
    # Leave stack pointer value unchanged, eg: don't overwrite RAM[0] (or whatever @SP is set to) value
    # and jumps to the @SP register value - 1
    self.lines.append("A=M-1")
