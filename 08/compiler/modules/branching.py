class Branching:
  def __init__(self, *, stack):
    self.lines = stack.lines
    self.stack = stack

  def run(self, command, label):
    if command == "label":
      self._label(label)
    elif command == "goto":
      self._goto(label)
    elif command == "if-goto":
      self._if_goto_label(label)

  def _label(self, label):
    self.lines.append("({}${})".format(self.stack.current_function_name, label))

  def _goto(self, label):
    self.lines.append("@{}${}".format(self.stack.current_function_name, label))
    self.lines.append("0;JMP")

  def _if_goto_label(self, label):
    self.stack.pop()
    self.lines.append("D=M")
    self.lines.append("@{}${}".format(self.stack.current_function_name, label))
    self.lines.append("D;JNE")
