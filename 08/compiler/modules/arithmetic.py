class Arithmetic:
  def __init__(self, *, stack):
    self.lines = stack.lines
    self.stack = stack

  def run(self, command):
    operation = {
      'neg': self._neg,
      'not': self._not,
      'add': self.__add,
      'sub': self.__sub,
      'eq': self.__eq,
      'lt': self.__lt,
      'gt': self.__gt,
      'and': self.__and,
      'or': self.__or
    }[command]

    if command in ['add', 'sub']:
      self._arithmetic(operation)
    elif command in ['and', 'or']:
      self._logical(operation)
    elif command in [ 'eq', 'lt', 'gt']:
      self._comparison(operation)
    else:
      operation()

  def __and(self):
    self.lines.append("M=D&M")

  def __or(self):
    self.lines.append("M=D|M")

  def _logical(self, func):
    # Get first value
    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")
    self.stack.get()

    # logical operation
    func()

  def __eq(self):
    self.lines.append("D;JEQ")

  def __lt(self):
    self.lines.append("D;JLT")

  def __gt(self):
    self.lines.append("D;JGT")

  def _comparison(self, func):
    # Get first value
    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")
    self.stack.get()

    # Set CPU D register equal to the difference of the: second value - first value
    # later, in the func() this value can be compared whether it is greater, equal or less then 0
    self.lines.append("D=M-D")
    # Assume the condition will evaluated to true
    self.lines.append("M=-1")
    # Set target label address to jump to if condition evaluated true
    self.lines.append("@{}$comparison.{}".format(self.stack.current_function_name, self.stack.label_idx))

    # comparison
    func()

    # if these lines aren't skipped using the label,
    # that means the condition evaluated to false
    # Read stack pointer value
    self.lines.append("@SP")
    # Go to its target + 1 becuause pop moved the pointer 1 further down
    self.lines.append("A=M-1")
    # Set memory register to equal to false
    self.lines.append("M=0")

    # Create a label to jump to if the condition evaluated true,
    # even though this line is later in the file, it will be parsed by the
    # assembler in the first pass, while the rest of the code will be parsed
    # in the second pass
    self.lines.append("({}$comparison.{})".format(self.stack.current_function_name, self.stack.label_idx))
    self.stack.label_idx += 1

  def __add(self):
    # Add the Data register and the memory register value together and store it in the data register
    self.lines.append("D=D+M")

  def __sub(self):
    # Add the Data register and the memory register value together and store it in the data register
    self.lines.append("D=M-D")

  def _arithmetic(self, func):
    # Get first value
    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")
    # Get second value
    self.stack.pop()
    # compute
    func()
    # push result back to memory and update stack pointer
    self.stack.push()

  def _neg(self):
    self.stack.get()
    # Write back negated value to the register directly
    # this way doing push in one step
    self.lines.append("M=-M")

  def _not(self):
    self.stack.get()
    # Write back not value to the register directly
    # this way doing push in one step
    self.lines.append("M=!M")
