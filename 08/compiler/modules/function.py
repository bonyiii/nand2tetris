from modules.memory_segment import MemorySegment

class Function:
  def __init__(self, *, stack):
    self.lines = stack.lines
    self.stack = stack

  def run(self, command, function_name, variable_count):
    if command == "function":
      self.stack.current_function_name = function_name
      self._function(variable_count)
    elif command == "return":
      self._return()
    elif command == "call":
      self._call(function_name, variable_count)

  def _function(self, local_variables_count):
    self.lines.append("({})".format(self.stack.current_function_name))
    for i in range(local_variables_count):
      self.lines.append("@SP")
      self.lines.append("A=M")
      self.lines.append("M=0")
      self.lines.append("@SP")
      self.lines.append("M=M+1")

  def _return(self):
    # endframe = LCL
    self.lines.append("@LCL // LCL")
    self.lines.append("D=M")
    self.lines.append("@{}$endframe".format(self.stack.current_function_name))
    self.lines.append("M=D")

    # retAddr = *(endframe - 5)
    self.lines.append("@5 // retAddr")
    self.lines.append("A=D-A")
    self.lines.append("D=M")
    self.lines.append("@{}$retaddr".format(self.stack.current_function_name))
    self.lines.append("M=D")

    # *ARG = pop()
    self.stack.pop()
    self.lines.append("D=M // *ARG")
    self.lines.append("@ARG")
    self.lines.append("A=M")
    self.lines.append("M=D")

    # SP = ARG + 1
    self.lines.append("@ARG // SP = ARG + 1")
    self.lines.append("D=M")
    self.lines.append("@SP")
    self.lines.append("M=D+1")

    # THAT = *(endframe - 1)
    self.__restore("THAT")

    # THIS = *(endframe - 2)
    self.__restore("THIS")

    # ARG = *(endframe - 3)
    self.__restore("ARG")

    # LCL = *(endframe - 4)
    self.__restore("LCL")

    # goto retAddr
    self.lines.append("@{}$retaddr".format(self.stack.current_function_name))
    self.lines.append("A=M")
    self.lines.append("0;JMP")

  def __restore(self, item):
    self.lines.append("@{}$endframe // restore {}".format(self.stack.current_function_name, item))
    self.lines.append("AM=M-1")
    self.lines.append("D=M")
    self.lines.append("@{}".format(item))
    self.lines.append("M=D")

  def _call(self, function_name, arguments_count):
    # push returnAddress
    self.lines.append("@{}$retaddr.{}".format(function_name, self.stack.label_idx))
    self.lines.append("D=A")
    self.stack.push()

    # push LCL
    self.__push("LCL")

    # push ARG
    self.__push("ARG")

    # push THIS
    self.__push("THIS")

    # push THAT
    self.__push("THAT")

    # ARG = SP - 5 - arguments_count
    self.lines.append("@{} // set ARG".format(arguments_count + 5))
    self.lines.append("D=A")
    self.lines.append("@SP")
    self.lines.append("D=M-D")
    self.lines.append("@ARG")
    self.lines.append("M=D")

    # LCL = SP
    self.lines.append("@SP // set LCL")
    self.lines.append("D=M")
    self.lines.append("@LCL")
    self.lines.append("M=D")

    # goto functionName
    self.lines.append("@{}".format(function_name))
    self.lines.append("0;JMP")

    self.lines.append("({}$retaddr.{})".format(function_name, self.stack.label_idx))
    self.stack.label_idx += 1

  def __push(self, item):
    self.lines.append(f"@{item} // push {item}")
    self.lines.append("D=M")
    self.stack.push()
