class MemorySegment:
  def __init__(self, *, stack):
    self.lines = stack.lines
    self.stack = stack
    self.segments = {
      'local': "LCL",
      'argument': "ARG",
      'this': "THIS",
      'that': "THAT",
      'temp': 5,
      'pointer': 3,
      'static': 16
    }

  def pop(self, segment, index):
    fixed_address = False
    if segment == 'static':
      self._save_static(index)
    else:
      if segment in ['temp', 'pointer']:
        fixed_address = True
      self._save_segment(self.segments[segment], index, fixed_address)

  def _save_static(self, index):
    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")
    self.lines.append("@{}.{}".format(self.stack.static_namespace, index))
    self.lines.append("M=D")

  def _save_segment(self, segment, index, fixed_address):
    # Goto memory segment base address
    self.lines.append("@{}".format(segment))

    if fixed_address:
      # Load address into CPU D register (temp is fixed at address 3)
      self.lines.append("D=A")
    else:
      # Load A(address) value stored in memory segment (M) into CPU D register, eg: (*SEGMENT)
      self.lines.append("D=M")

    # Goto index
    self.lines.append("@{}".format(index))
    # Write index + segment base adress into CPU D(ata) register
    self.lines.append("D=D+A")
    # Goto R13 general purpose register
    self.lines.append("@R13")
    # Save segment + index address in @R13
    self.lines.append("M=D")

    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")

    # Goto R13 general purpose register, read its value and jump there (eg: *R13)
    self.lines.append("@R13")
    self.lines.append("A=M")

    # Load CPU D register value into memory M register
    self.lines.append("M=D")

  def push(self, segment, index):
    if segment == 'constant':
      self._load_constant(index)
    elif segment == 'static':
      self._load_static(index)
    else:
      fixed_address = False
      if segment in ['temp', 'pointer']:
        fixed_address = True
      self._load_segment(self.segments[segment], index, fixed_address)

  def _load_segment(self, segment, index, fixed_address):
    # Go to index
    self.lines.append("@{}".format(index))
    # Load index address into CPU D register
    self.lines.append("D=A")
    # Go to this
    self.lines.append("@{}".format(segment))

    if fixed_address:
      # Set address to: index + address
      self.lines.append("A=D+A")
    else:
      # Set address to: index + *this (eg: offset *this (value) by the index value)
      self.lines.append("A=D+M")

    # Load memory register M value into CPU D register
    self.lines.append("D=M")
    self.stack.push()

  def _load_static(self, index):
    self.lines.append("@{}.{}".format(self.stack.static_namespace, index))
    self.lines.append("D=M")
    self.stack.push()

  def _load_constant(self, index):
    # Set A register to the value of the selected memory segment and index
    self.lines.append("@{}".format(index))
    # Load address into CPU data register
    self.lines.append("D=A")
    self.stack.push()
