import re

class Code:
  def __init__(self, symbol_table):
    self.code = []
    self.symbol_table = symbol_table

  def a_command(self, parser):
    #variable = re.search(r'[a-zA-Z_]+(\d+)?', parser.current_command)
    digit = re.search(r'^@\d+$', parser.current_command)

    if digit:
      ram_address = parser.symbol()
    else:
      ram_address = self.symbol_table[parser.symbol()]
      if ram_address == None:
        ram_address = self.symbol_table.add_variable(parser.symbol())

    self.code.append('{:016b}'.format(int(ram_address)))

  def c_command(self, parser):
    dest = self._dest(parser.dest())
    comp = self._comp(parser.comp())
    jump = self._jump(parser.jump())

    command = '111'
    if comp:
      command += comp
    if dest:
      command += dest
    if jump:
      command += jump

    # if parser.current_line == 6:
    #  print(parser.current_command)
    #  print("111", comp, dest, jump)
    #  print(command, (parser._c_command()))
    self.code.append(command)

  def _dest(self, key):
    bits = {
        "M": "001",
        "D": "010",
       "MD": "011",
        "A": "100",
       "AM": "101",
       "AD": "110",
      "AMD": "111"
    }
    return bits.get(key, '000')

  def _comp(self, key):
    # The first column is the a bit
    # the rest of the columns represent the c bits from 1 to 6
    #         ac1c2vc3c4c5c6
    #         a123456
    bits = {
        "0": "0101010",
        "1": "0111111",
       "-1": "0111010",
        "D": "0001100",
        "A": "0110000",        "M": "1110000",
       "!D": "0001101",
       "!A": "0110001",       "!M": "1110001",
       "-D": "0000111",
       "-A": "0110011",       "-M": "1110011",
      "D+1": "0011111",
      "A+1": "0110111",      "M+1": "1110111",
      "D-1": "0001110",
      "A-1": "0110010",      "M-1": "1110010",
      "D+A": "0000010",      "D+M": "1000010",
      "D-A": "0010011",      "D-M": "1010011",
      "A-D": "0000111",      "M-D": "1000111",
      "D&A": "0000000",      "D&M": "1000000",
      "D|A": "0010101",      "D|M": "1010101"
    }
    return bits.get(key, "0000000")

  def _jump(self, key):
    bits = {
      "JGT": "001",
      "JEQ": "010",
      "JGE": "011",
      "JLT": "100",
      "JNE": "101",
      "JLE": "110",
      "JMP": "111"
    }
    return bits.get(key, '000')

