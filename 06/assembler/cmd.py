import re
import argparse
from parser import Parser
from code import Code
from symbol_table import SymbolTable

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-i', type=str, help='Source .asm file')
parser.add_argument('-o', type=str, help='Generated binary code file')

args = parser.parse_args()
input_file = args.i
output_file = args.o


class Cmd:
  def __init__(self):
    self.lines = []
    self.not_empty_line_pattern = re.compile(r"\S+")
    self.symbol_table = SymbolTable()
    self.code = Code(self.symbol_table)

  def is_code(self, line):
    return re.search(self.not_empty_line_pattern, line)

  def strip_comments(self, line):
    return line.split('//')[0]

  def read(self, input_file):
    with open(input_file) as f:
      for line in f:
        line = self.strip_comments(line)
        if self.is_code(line):
          self.lines.append(line.strip())

  def first_pass(self):
    parser = Parser(self.symbol_table, self.lines)
    rom_address = 0
    while parser.has_more_commands():
      parser.advance()
      if parser.command_type() == Parser.L_COMMAND:
        self.symbol_table.add_symbol(parser.current_command[1:-1], rom_address)
      else:
        rom_address += 1

  def second_pass(self):
    parser = Parser(self.symbol_table, self.lines)
    while parser.has_more_commands():
      parser.advance()
      if parser.command_type() == Parser.A_COMMAND:
        self.code.a_command(parser)
      elif parser.command_type() == Parser.L_COMMAND:
        pass
      else:
        self.code.c_command(parser)

  def write(self, output_file):
    with open(output_file, "w") as f:
      for line in self.code.code:
        f.write(line + "\n")


cmd = Cmd()
cmd.read(input_file)
cmd.first_pass()
print(cmd.symbol_table.symbols)
cmd.second_pass()
cmd.write(output_file)
