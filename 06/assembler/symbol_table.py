class SymbolTable:
    def __init__(self):
        self.init_symbols()
        self.ram_address=15

    def init_symbols(self):
        self.symbols = {
            "SP": 0,
            "LCL": 1,
            "ARG": 2,
            "THIS": 3,
            "THAT": 4,
            "SCREEN": 16384,
            "KBD": 24576
        }
        for i in range(16):
            self.symbols["R"+str(i)] = i

    def add_symbol(self, key, rom_address):
        self.symbols[key] = rom_address

    def add_variable(self, key):
        self.ram_address += 1
        self.symbols[key] = self.ram_address
        return self.ram_address

    def __getitem__(self, key):
        return self.symbols.get(key)
