import re

class Parser:
    A_COMMAND='A'
    C_COMMAND='C'
    L_COMMAND='L'

    def __init__(self, symbol_table, lines):
        self.current_line = 0
        self.current_command = None
        self.lines = lines
        self.symbol_table = symbol_table
        self.c_command_pattern = re.compile(r'(\w+)?(?:=)?([\w\-+&|!]+)?(?:;)?(\w+)?')

    def command_type(self):
        if self.current_command[0] == "@":
            return self.A_COMMAND
        elif self.current_command[0] == "(":
            return self.L_COMMAND
        else:
            return self.C_COMMAND

    def has_more_commands(self):
        return self.current_line < len(self.lines)

    def advance(self):
        if self.has_more_commands():
            self.current_command = self.lines[self.current_line]
            self.current_line += 1

    def symbol(self):
        if self.current_command[0] == "@":
            return self._register()
        else:
            return self._label()

    def dest(self):
        if "=" in self.current_command:
            return self._c_command().group(1)

    def comp(self):
        if "=" in self.current_command:
            return self._c_command().group(2)
        else:
            return self._c_command().group(1)

    def jump(self):
        return self._c_command().group(3)

    def _c_command(self):
        return re.match(self.c_command_pattern, self.current_command)

    def _register(self):
        return self.current_command[1:]

    def _label(self):
        return self.current_command[1:-1]

