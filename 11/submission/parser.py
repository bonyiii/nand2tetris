import os
import sys
from symbol_table import SymbolTable, SymbolNotFoundError
from vm_writer import VmWriter

class ParseError(BaseException):
  def __init__(self, token, expected, file_name, parser):
    self.message = parser.debug()
    self.message += "\n####\n"
    self.message += "ParseError:\n File \"{file_name}\", line {line_no} \nrecevied token: {token} is not expected. \nexpected token: {expected}".format(file_name=file_name, line_no=token.line_no, token=token.to_xml(), expected=expected)


class UnexpectedTokenError(BaseException):
  def __init__(self, expected_type, expected_value):
    self.message = "No more tokens left! \"{}\" is missing.".format(" ".join(expected_type) + " ". join(expected_value))

class Parser:
  STATEMENTS = ['let', 'if', 'while', 'do', 'return']

  def __init__(self, tokenizer):
    self.tokenizer = tokenizer
    self.label_idx = 0
    self.class_symbol_table = SymbolTable()
    self.vm_writer = VmWriter()

  def next_label_idx(self):
    idx = self.label_idx
    self.label_idx += 1
    return idx


  def debug(self):
    message = "\n####### VM CODE ###########\n"
    message += self.vm_writer.code
    message += "\n####### VM CODE ###########\n"
    return message


  def write(self, output_file):
    self.vm_writer.save(output_file)

  # type
  # className
  # subroutineName
  # variableName
  # statement
  # subroutineCall

  def next_token(self):
    if self.tokenizer.has_more_tokens():
      token = self.tokenizer.advance()
      #print(token.to_xml(), "next")
      return token


  def eat(self, expected_value = [], expected_type = [], token = None, sym_table = None, kind = None, type_of = None, usage = None):
    l_token = token or self.next_token()
    if l_token:
      if token:
        print(l_token.to_xml(), "current")
      elif l_token:
        print(l_token.to_xml(), "next")
      if (l_token.value in expected_value) or (l_token.token_type in expected_type):
        if kind:
          if kind in ['var', 'argument', 'static', 'field']:
            if kind == 'var':
              kind = 'local'
            if usage == 'defined':
              #print("??????? VAR DEF ???????: ", l_token.value, type_of, kind)
              sym_table.define(l_token.value, type_of, kind)

        return l_token

      try:
        raise(ParseError(l_token, (expected_value + expected_type), self.tokenizer.file_name, self))
      except ParseError as e:
        print(e.message)

      raise(ValueError("Token mismatch"))
    else:
      raise ValueError("No more tokens to eat!")


  def compile_class(self):
    self.eat(expected_value=['class'])
    self.klass_name = self.eat(expected_type=['identifier']).value
    self.eat(expected_value=['{'])

    token = self.compile_class_var_dec()

    token = self.eat(expected_value = ['constructor', 'function', 'method', '}'], token = token)
    while token.value != "}":
      self.compile_subroutine_dec(token)
      token = self.eat(expected_value = ['constructor', 'function', 'method', '}'])

    print(self.debug())


  def compile_class_var_dec(self):
    while True:
      token = self.next_token()
      if (token.value in ['static', 'field']):
        # static | field
        kind = token.value
        self.eat(expected_value = ['static', 'field'], token = token)
        # type
        token = self.eat(expected_value = ['int', 'char', 'boolean'], expected_type = ['identifier'])
        type_of = token.value
        # varName*
        self._one_or_more_identifier(self.class_symbol_table, kind, type_of)
      else:
        return token


  def _one_or_more_identifier(self, sym_table, kind, type_of):
    while True:
      self.eat(expected_type=['identifier'], sym_table = sym_table, kind = kind, type_of = type_of, usage = 'defined')
      token = self.next_token()
      if token.value in [',']:
        self.eat(expected_value=[','], token = token)
      else:
        self.eat(expected_value=[';'], token = token)
        break


  def compile_subroutine_dec(self, token):
    self.if_label_idx = 0
    self.while_label_idx = 0
    symbol_table = SymbolTable(parent_symbol_table = self.class_symbol_table)

    fun_type = self.eat(expected_value = ['function', 'method', 'constructor'], token = token).value
    # void | type
    self.eat(expected_value = ['int', 'char', 'boolean', 'void'], expected_type = ['identifier'])
    fun_name = self.eat(expected_type = ['identifier'], kind = 'subroutine', usage = 'defined').value
    # parameterList
    param_count = self.compile_parameter_list(symbol_table, fun_type)
    fun_name = "{}.{}".format(self.klass_name, fun_name)

    self.compile_subroutine_body(symbol_table, fun_name, fun_type)


  def compile_parameter_list(self, sym_table, fun_type):
    self.eat(expected_value = ['('])

    if fun_type == 'method':
      sym_table.define('this', self.klass_name, 'argument')

    token = self.next_token()
    while token.value != ')':
      type_of = self.eat(expected_type=['keyword', 'identifier'], token = token).value # type
      self.eat(expected_type=['identifier'], sym_table = sym_table, kind = 'argument', type_of = type_of, usage = 'defined') # name

      token = self.next_token()
      if token.value in [',']:
        self.eat(expected_value = [','], token = token)
        token = self.next_token()

    self.eat(expected_value = [')'], token = token)


  def compile_subroutine_body(self, sym_table, fun_name, fun_type):
    self.eat(expected_value = ['{'])

    token = self.next_token()
    while token.value == 'var':
      if token.value in ['var']:
        self.compile_var_dec(token, sym_table)
        token = self.next_token()
    self.vm_writer.function(fun_name, sym_table.var_count('local'))

    if fun_type == 'constructor':
      self.vm_writer.push('constant', self.class_symbol_table.var_count('field'))
      self.vm_writer.call('Memory.alloc', 1)
      self.vm_writer.pop('pointer', 0)
    elif fun_type == 'method':
      self.vm_writer.push('argument', 0)
      self.vm_writer.pop('pointer', 0)

    token = self.compile_statements(token, sym_table)

    self.eat(expected_value = ['}'], token = token)


  def compile_var_dec(self, token, sym_table):
    # var
    self.eat(expected_value = ['var'], token = token)
    # type
    token = self.eat(expected_value = ['int', 'char', 'boolean'], expected_type = ['identifier'])
    type_of = token.value
    # varName*
    self._one_or_more_identifier(sym_table, 'var', type_of)


  def compile_statements(self, token, sym_table):
    while token.value in self.STATEMENTS:
      if token.value == 'let':
        self.compile_let(token, sym_table)
      elif token.value == 'if':
        token = self.compile_if(token, sym_table)
        continue
      elif token.value == 'do':
        self.compile_do(token, sym_table)
      elif token.value == 'return':
        self.compile_return(token, sym_table)
      elif token.value == 'while':
        self.compile_while(token, sym_table)
      token = self.next_token()

    return token

  def compile_let(self, token, sym_table):
    self.eat(expected_value = ['let'], token = token)
    variable = self.eat(expected_type = ['identifier']).value

    token = self.next_token()

    if token.value == '[':
      self.eat(expected_value = ['['], token = token)
      # expression will push variable
      token = self.compile_expression(sym_table = sym_table)
      # push array base address
      self.vm_writer.push(sym_table.kind_of(variable), sym_table.index_of(variable))
      # add them so that we seek to the proper array index
      self.vm_writer.arithmetic("add")
      self.eat(expected_value = [']'], token = token)
      self.eat(expected_value = ['='])
      # push expression value
      token = self.compile_expression(sym_table = sym_table)
      self.vm_writer.pop('temp', 0)
      self.vm_writer.pop('pointer', 1)
      self.vm_writer.push('temp', 0)
      self.vm_writer.pop('that', 0)
    else:
      self.eat(expected_value = ['='], token = token)
      token = self.compile_expression(sym_table = sym_table)
      kind_of = sym_table.kind_of(variable)
      segment = 'this' if kind_of == 'field' else kind_of
      self.vm_writer.pop(segment, sym_table.index_of(variable))

    self.eat(expected_value = [';'], token = token)


  def compile_if(self, token, sym_table):
    idx = self.if_label_idx
    self.if_label_idx += 1
    self.eat(expected_value = ['if'], token = token)
    self.eat(expected_value = ['('])
    token = self.compile_expression(sym_table = sym_table, kind = 'argument')
    self.eat(expected_value = [')'], token = token)
    self.vm_writer.if_goto("IF_TRUE", idx)
    self.vm_writer.goto("IF_FALSE", idx)
    self.vm_writer.label("IF_TRUE", idx)

    # if body
    self.eat(expected_value = ['{'])
    token = self.compile_statements(self.next_token(), sym_table)
    self.eat(expected_value = ['}'], token = token)



    # else body (potential)
    token = self.next_token()
    if token.value == "else":
      self.vm_writer.goto("IF_END", idx)
      self.vm_writer.label("IF_FALSE", idx)
      self.eat(expected_value = ['else'], token=token)
      # else body
      self.eat(expected_value = ['{'])
      token = self.compile_statements(self.next_token(),sym_table)
      self.eat(expected_value = ['}'], token = token)
      token = self.next_token()
      self.vm_writer.label("IF_END", idx)
    else:
      self.vm_writer.label("IF_FALSE", idx)

    return token


  def compile_while(self, token, sym_table):
    idx = self.while_label_idx
    self.while_label_idx += 1
    self.eat(expected_value = ['while'], token = token)
    self.vm_writer.label("WHILE_EXP", idx)
    self.eat(expected_value = ['('])
    token = self.compile_expression(sym_table = sym_table, kind = 'argument')
    self.eat(expected_value = [')'], token = token)
    self.vm_writer.arithmetic("not")
    self.vm_writer.if_goto('WHILE_END', idx)
    # while body
    self.eat(expected_value = ['{'])
    token = self.compile_statements(self.next_token(),sym_table)
    self.vm_writer.goto("WHILE_EXP", idx)
    self.eat(expected_value = ['}'], token = token)
    self.vm_writer.label("WHILE_END", idx)



  def compile_do(self, token, sym_table):
    self.eat(expected_value = ['do'], token = token)
    token = self.next_token()
    self.subroutine_call(sym_table, token = token)
    self.eat(expected_value = [';'])
    self.vm_writer.pop('temp', 0)


  def compile_return(self, token, sym_table):
    self.eat(expected_value = ['return'], token = token)

    token = self.next_token()
    if token.value == ';':
      self.eat(expected_value = [';'], token = token)
      self.vm_writer.push('constant', 0)
    else:
      token = self.compile_expression(token = token, sym_table = sym_table)
      self.eat(expected_value = [';'], token = token)


    self.vm_writer.write_return('')


  def compile_expression(self, token = None, sym_table = None, kind = 'var'):
    token = token or self.next_token()
    term_next_token = self.compile_term(token, sym_table, kind = kind)

    # (exp op exp)
    while True:
      term_next_token = term_next_token or self.next_token()
      # op
      if term_next_token.value in ['+', '-', '*', '/', '&', '|', '<', '>', '=']:
        # eats term_next_token
        op = self.eat(expected_type = ['symbol'], token = term_next_token).value
        # term
        term_next_token = None
        term_next_token = self.compile_term(self.next_token(), sym_table, kind = kind)
        self.vm_writer.op(op)
      else:
        return term_next_token


  def compile_term(self, token, sym_table, kind = 'var'):
    term_next_token = self.next_token()

    # varName[expression]
    if term_next_token.value in ['[']:
      variable = self.eat(expected_type = ['identifier', 'keyword'], token = token, sym_table = sym_table).value
      # eats term_next_node
      self.eat(expected_value = ['['], token = term_next_token)
      token = self.compile_expression(sym_table = sym_table)
      self.vm_writer.push(sym_table.kind_of(variable), sym_table.index_of(variable))
      self.vm_writer.arithmetic("add")
      self.vm_writer.pop('pointer', 1)
      self.vm_writer.push('that', 0)
      self.eat(expected_value = [']'], token = token)
    # (expression)
    elif token.value == '(':
      self.eat(expected_value = ['('], token = token)
      # eats term_next_node
      token = self.compile_expression(token = term_next_token, sym_table = sym_table)
      self.eat(expected_value = [')'], token = token)
    # unaryOp term (op exp)
    elif token.value in ['-', '~']:
      op = self.eat(expected_type = ['symbol'], token = token).value
      # print exp
      term_result =  self.compile_term(term_next_token, sym_table)
      # print op
      if op == "-":
        self.vm_writer.arithmetic("neg")
      else:
        self.vm_writer.arithmetic("not")
      return term_result
    # subrutineCall
    elif term_next_token.value in ['(', '.']:
      self.subroutine_call(sym_table, token = token, term_next_token = term_next_token)
    # varName or constants
    else:
      if token.token_type == 'identifier':
        #sym_table.debug()
        variable= self.eat(expected_type = ['identifier'], token = token).value
        kind_of = sym_table.kind_of(variable)
        segment = 'this' if kind_of == 'field' else kind_of
        self.vm_writer.push(segment, sym_table.index_of(variable))
      else:
        self.eat(expected_type = ['integerConstant', 'stringConstant', 'keyword'], token = token)
        if token.token_type == 'keyword':
          self.handle_keyword_term(token)
        elif token.token_type == "stringConstant":
          self.handle_string_term(token)
        else:
          self.vm_writer.push('constant', token.value)

      return term_next_token

  def handle_string_term(self, token):
    self.vm_writer.push("constant", len(token.value))
    self.vm_writer.call("String.new", 1)
    for l in token.value:
      self.vm_writer.push("constant", ord(l))
      self.vm_writer.call("String.appendChar", 2)

  def handle_keyword_term(self, token):
    if token.value == 'this':
      self.vm_writer.push('pointer', 0)
    elif token.value == "true":
      self.vm_writer.push('constant', 0)
      self.vm_writer.arithmetic('not')
    elif token.value in ["false", "null"]:
      self.vm_writer.push('constant', 0)



  def compile_expression_list(self, sym_table = None, segment = None, obj_idx = 0):
    argument_count = 0

    if segment:
      self.vm_writer.push(segment, obj_idx)
      argument_count += 1

    while True:
      token = self.next_token()

      if token.value == ",":
        self.eat(expected_value=[','], token = token)
      elif token.value == ")":
        return token, argument_count
      else:
        token = self.compile_expression(token, sym_table = sym_table)
        argument_count += 1
        if token.value == ")":
          return token, argument_count


  # f(exp1, exp2, exp3)
  def subroutine_call(self, sym_table, *, token, term_next_token = None):
    # method call on implicit this
    fun_name = self.eat(expected_type = ['identifier'], token = token, sym_table = sym_table).value

    token = term_next_token if term_next_token else self.next_token()

    if token.value == '(':
      self.eat(expected_value = ['('], token = token)
      token, argument_count = self.compile_expression_list(sym_table = sym_table, segment = 'pointer', obj_idx = 0)
      self.eat(expected_value = [')'], token = token)
      klass_name = self.klass_name
      fun_name = "{}.{}".format(klass_name, fun_name)
    # method call on an object or on a different class
    elif token.value == '.':
      self.eat(expected_value = ['.'], token = token).value
      token = self.next_token()
      method = self.eat(expected_type = ['identifier'], token = token).value
      self.eat(expected_value = ['('])

      segment = None
      obj_idx = 0
      try:
        # object found in symbol table then use its class and call method on it and pass this as first argument
        obj = sym_table.type_of(fun_name)
        kind_of = sym_table.kind_of(fun_name)
        obj_idx = sym_table.index_of(fun_name)
        segment = 'this' if kind_of == 'field' else kind_of
        fun_name = "{}.{}".format(obj, method)
      except SymbolNotFoundError:
        # call class method on a different class
        fun_name = "{}.{}".format(fun_name, method)

      token, argument_count = self.compile_expression_list(sym_table = sym_table, segment = segment, obj_idx = obj_idx)
      self.eat(expected_value = [')'], token = token)
    self.vm_writer.call(fun_name, argument_count)
