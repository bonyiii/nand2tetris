class SymbolNotFoundError(BaseException):
    def __init(self, msg):
        self.message = msg

class SymbolTable:
    KIND = ['STATIC', 'FIELD', 'ARGS', 'VAR']

    def __init__(self, parent_symbol_table = None):
        self.table = {}
        self.parent = parent_symbol_table

    def define(self, name, type_of, kind):
        self.table[name] = { 'type_of': type_of, 'idx': self.var_count(kind), 'kind': kind }
        return self.index_of(name)

    def var_count(self, kind):
        return len([1 for i in self.table.values() if i['kind'] == kind])

    def kind_of(self, name):
        try:
            return self.table[name]['kind']
        except:
            if self.parent:
                return self.parent.kind_of(name)
            else:
                raise SymbolNotFoundError("No kind for undefined variable '{}' in symbol table: ".format(name))

    def type_of(self, name):
        try:
            return self.table[name]['type_of']
        except:
            if self.parent:
                return self.parent.type_of(name)
            else:
                raise SymbolNotFoundError("No type for undefined variable '{}' in symbol table: ".format(name))

    def index_of(self, name):
        try:
            return self.table[name]['idx']
        except:
            if self.parent:
                return self.parent.index_of(name)
            else:
                raise SymbolNotFoundError("No index for undefined variable '{}' in symbol table: ".format(name))

    def debug(self):
        print("\n##### SYMBOL TABLE ########\n")
        for key, value in self.table.items():
            print(key, value)
            print("\n##### SYMBOL TABLE ########\n")
