import unittest
import pdb
from textwrap import wrap

from parser import Parser
from tokenizer import Token
from symbol_table import SymbolTable

if 'unittest.util' in __import__('sys').modules:
      # Show full diff in self.assertEqual.
      __import__('sys').modules['unittest.util']._MAX_LENGTH = 999999999

class Tokenizer:
  def __init__(self):
    self.tokens = []
    self.file_name = 'FakeFile'

  def has_more_tokens(self):
    return len(self.tokens) > 0

  def advance(self):
    return self.tokens.pop()

  def next_token(self):
    if self.has_more_tokens():
      return self.advance()


class ParserTest(unittest.TestCase):
  def setUp(self):
    self.maxDiff = None

    self.tokenizer = Tokenizer()
    self.parser = Parser(self.tokenizer)


  def test_compile_class(self):
    self.tokenizer.tokens.append(Token('class', 'keyword', 1))
    self.tokenizer.tokens.append(Token('Game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_class()
    self.assertEqual(self.parser.vm_writer.code, "")


  def test_compile_class_var_dec(self):
    self.tokenizer.tokens.append(Token('field', 'keyword', 1))
    self.tokenizer.tokens.append(Token('int', 'keyword', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    closing_curly_token = Token('}', 'symbol', 1)
    self.tokenizer.tokens.append(closing_curly_token)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_class_var_dec(self.root_node)
    self.assertEqual(self.parser.vm_writer.code, "")

    self.assertEqual(self.parser.class_symbol_table.table,
                 {'x': {'idx': 0, 'type_of': 'int', 'kind': 'field'},
                  'y': {'idx': 1, 'type_of': 'int', 'kind': 'field'}})

  def test_compile_class_var_dec_with_custom_type(self):
    self.tokenizer.tokens.append(Token('field', 'keyword', 1))
    self.tokenizer.tokens.append(Token('Blah', 'identifier', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    closing_curly_token = Token('}', 'symbol', 1)
    self.tokenizer.tokens.append(closing_curly_token)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_class_var_dec(self.root_node)
    self.assertEqual(ET.tostring(self.root_node).decode('utf-8'), '<test_root><classVarDec>\n<keyword>field</keyword>\n<identifier>Blah</identifier>\n<identifier idx="0" kind="field" usage="defined">x</identifier>\n<symbol>;</symbol>\n</classVarDec>\n</test_root>')


  def test_compile_var_dec(self):
    symbol_table = SymbolTable()
    token = Token('var', 'keyword', 1)
    self.tokenizer.tokens.append(Token('SquareGame', 'identifier', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_var_dec(token, symbol_table)
    self.assertEqual(self.parser.vm_writer.code, "")

    self.assertEqual(symbol_table.table,
                 {'x': {'idx': 0, 'type_of': 'SquareGame', 'kind': 'local'},
                  'y': {'idx': 1, 'type_of': 'SquareGame', 'kind': 'local'}})

  def test_one_or_more_identifier(self):
    symbol_table = SymbolTable()
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser._one_or_more_identifier(self.root_node, symbol_table, 'argument', 'Point')
    self.assertEqual(ET.tostring(self.root_node).decode('utf-8'), '<test_root><identifier idx="0" kind="argument" usage="defined">x</identifier>\n<symbol>,</symbol>\n<identifier idx="1" kind="argument" usage="defined">y</identifier>\n<symbol>;</symbol>\n</test_root>')

  def test_compile_subroutine_dec(self):
    self.parser.klass_name="HelloWorld"
    token = Token('constructor', 'keyword', 1)
    self.tokenizer.tokens.append(Token('Square', 'identifier', 1))
    self.tokenizer.tokens.append(Token('new', 'identifier', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('int', 'keyword', 1))
    self.tokenizer.tokens.append(Token('Ax', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_subroutine_dec(token)
    self.assertEqual(self.parser.vm_writer.code, "function HelloWorld.new 0\n")


  def test_compile_parameter_list_empty(self):
    symbol_table = SymbolTable()
    closing_parent = Token(')', 'symbol', 1)
    self.tokenizer.tokens.append(closing_parent)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_parameter_list(self.root_node, symbol_table)
    self.assertEqual(ET.tostring(self.root_node).decode('utf-8'), '<test_root><parameterList>\n</parameterList>\n</test_root>')
    self.assertEqual(result, closing_parent)

  def test_compile_parameter_list(self):
    symbol_table = SymbolTable()
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('int', 'keyword', 1))
    self.tokenizer.tokens.append(Token('Ax', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('int', 'keyword', 1))
    self.tokenizer.tokens.append(Token('Ay', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_parameter_list(symbol_table)
    self.assertEqual(self.parser.vm_writer.code, "")
    self.assertEqual(symbol_table.table,
                 {'Ax': {'idx': 0, 'type_of': 'int', 'kind': 'argument'},
                  'Ay': {'idx': 1, 'type_of': 'int', 'kind': 'argument'}})


  def test_compile_subroutine_body_with_var_dec(self):
    symbol_table = SymbolTable()
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('var', 'keyword', 1))
    self.tokenizer.tokens.append(Token('SquareGame', 'identifier', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_subroutine_body(symbol_table, "my_fun")
    self.assertEqual(self.parser.vm_writer.code, "function my_fun 2\n")
    self.assertEqual(symbol_table.table,
                 {'x': {'idx': 0, 'type_of': 'SquareGame', 'kind': 'local'},
                  'y': {'idx': 1, 'type_of': 'SquareGame', 'kind': 'local'}})

  def test_compile_subroutine_body_with_statements(self):
    symbol_table = SymbolTable()
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('return', 'keyword', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_subroutine_body(symbol_table, "my_fun")
    self.assertEqual(self.parser.vm_writer.code, "function my_fun 0\nreturn \n")

  def test_compile_let(self):
    symbol_table = SymbolTable()
    symbol_table.define('x', 'Whatever', 'local')
    symbol_table.define('a', 'Whatever', 'local')
    symbol_table.define('b', 'Whatever', 'local')
    symbol_table.define('c', 'Whatever', 'local')
    token = Token('let', 'keyword', 1)
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token('=', 'symbol', 1))
    self.tokenizer.tokens.append(Token('a', 'identifier', 1))
    self.tokenizer.tokens.append(Token('+', 'symbol', 1))
    self.tokenizer.tokens.append(Token('b', 'identifier', 1))
    self.tokenizer.tokens.append(Token('-', 'symbol', 1))
    self.tokenizer.tokens.append(Token('c', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_let(token, symbol_table)
    vm_code = """push local 1
push local 2
add
push local 3
neg
pop local 0
"""
    self.assertEqual(self.parser.vm_writer.code, vm_code)


  def test_compile_do(self):
    symbol_table = SymbolTable()
    token = Token('do', 'keyword', 1)
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('.', 'symbol', 1))
    self.tokenizer.tokens.append(Token('run', 'identifier', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_do(self.root_node, token, symbol_table)


  # do game.run(this);
  def test_compile_do_with_keyword(self):
    symbol_table = SymbolTable()
    token = Token('do', 'keyword', 1)
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('.', 'symbol', 1))
    self.tokenizer.tokens.append(Token('run', 'identifier', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('this', 'keyword', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_do(self.root_node, token, symbol_table)


  def test_compile_if(self):
    symbol_table = SymbolTable()
    symbol_table.define('game', 'Whatever', 'var')
    token = Token('if', 'keyword', 1)
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    # There must be something that wraps the if statement (like function body), so assuming there is a closing curly brace is safe
    # so this brace is only here to emulate that
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_if(self.root_node, token, symbol_table)


  def test_compile_nested_if_else(self):
    symbol_table = SymbolTable()
    symbol_table.define('game', 'Whatever', 'var')
    symbol_table.define('nested', 'int', 'var')
    token = Token('if', 'keyword', 1)
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))

    # if/else within an if
    self.tokenizer.tokens.append(Token('if', 'keyword', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('nested', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens.append(Token('else', 'keyword', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))

    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_if(self.root_node, token, symbol_table)


  def test_compile_else(self):
    symbol_table = SymbolTable()
    token = Token('else', 'keyword', 1)
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_else(self.root_node, token, symbol_table)


  def test_compile_else_with_statements(self):
    symbol_table = SymbolTable()
    token = Token('else', 'keyword', 1)
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('return', 'keyword', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_else(self.root_node, token, symbol_table)


  def test_compile_while(self):
    symbol_table = SymbolTable()
    symbol_table.define('game', 'Whatever', 'var')
    token = Token('while', 'keyword', 1)
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_while(self.root_node, token, symbol_table)


  def test_compile_subroutine_call_with_args(self):
    symbol_table = SymbolTable()
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('.', 'symbol', 1))
    # TODO exression
    self.tokenizer.tokens.append(Token('run', 'identifier', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('something', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('number', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.subroutine_call(symbol_table)
    #ET.dump(self.root_node)
    vm_code = """
call my_fun 2
return
"""
    self.assertEqual(self.parser.vm_writer.code, vm_code)


  def test_compile_expression_list(self):
    symbol_table = SymbolTable()
    closing_parent = Token(')', 'symbol', 1)
    self.tokenizer.tokens.append(closing_parent)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_expression_list(self.root_node, symbol_table)



  # (good, game)
  # the opening parenthesis parsed earlier
  def test_compile_expression_list_with_multiple_expressions(self):
    symbol_table = SymbolTable()
    symbol_table.define('good', 'bool', 'var')
    symbol_table.define('game', 'Whatever', 'var')
    closing_parent = Token(')', 'symbol', 1)
    self.tokenizer.tokens.append(Token('good', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(closing_parent)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_expression_list(self.root_node, symbol_table)


  def test_compile_return_with_var(self):
    symbol_table = SymbolTable()
    symbol_table.define('game', 'Whatever', 'var')
    token = Token('return', 'keyword', 1)
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_return(self.root_node, token, symbol_table)


  def test_compile_plain_return(self):
    symbol_table = SymbolTable()
    token = Token('return', 'keyword', 1)
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_return(self.root_node, token, symbol_table)



  def test_compile_term_string(self):
    symbol_table = SymbolTable()
    token = Token('good thing', 'stringConstant', 1)
    semicolon_token = Token(';', 'symbol', 1)
    self.tokenizer.tokens.append(semicolon_token)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_term(self.root_node, token, symbol_table)


  def test_compile_term_integer(self):
    symbol_table = SymbolTable()
    token = Token('27', 'integerConstant', 1)
    semicolon_token = Token(';', 'symbol', 1)
    self.tokenizer.tokens.append(semicolon_token)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_term(self.root_node, token, symbol_table)


  def test_compile_term_array(self):
    symbol_table = SymbolTable()
    token = Token('a', 'identifier', 1)
    self.tokenizer.tokens.append(Token('[', 'symbol', 1))
    self.tokenizer.tokens.append(Token('2', 'integerConstant', 1))
    self.tokenizer.tokens.append(Token(']', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_term(self.root_node, token, symbol_table)


  # i = i * (-j);
  def test_compile_expression(self):
    symbol_table = SymbolTable()
    symbol_table.define('i', 'int', 'var')
    symbol_table.define('j', 'int', 'var')
    self.tokenizer.tokens.append(Token('i', 'identifier', 1))
    self.tokenizer.tokens.append(Token('=', 'symbol', 1))
    self.tokenizer.tokens.append(Token('i', 'identifier', 1))
    self.tokenizer.tokens.append(Token('*', 'symbol', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('-', 'symbol', 1))
    self.tokenizer.tokens.append(Token('j', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_expression(self.root_node, sym_table = symbol_table)


    #while (~exit) {
  def test_compile_while_with_unary_expression(self):
    symbol_table = SymbolTable()
    symbol_table.define('game', 'Whatever', 'var')
    token = Token('while', 'keyword', 1)
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('-', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_while(token, symbol_table)
    self.assertEqual(self.parser.vm_writer.code, "push var 0\nneg\n")


    # if (((y + size) < 254) & ((x + size) < 510)) {
    # This is the expression part: ((y + size) < 254) & ((x + size) < 510)
    # expression(term op term)
  def test_compile_expression_nested(self):
    symbol_table = SymbolTable()
    symbol_table.define('y', 'int', 'var')
    symbol_table.define('size', 'int', 'var')
    symbol_table.define('x', 'int', 'var')
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token('+', 'symbol', 1))
    self.tokenizer.tokens.append(Token('size', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('<', 'symbol', 1))
    self.tokenizer.tokens.append(Token('254', 'integerConstant', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('&', 'symbol', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token('+', 'symbol', 1))
    self.tokenizer.tokens.append(Token('size', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('<', 'symbol', 1))
    self.tokenizer.tokens.append(Token('510', 'integerConstant', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    # Last closing parent for the if ) this is not going to be processed by the expression
    # but parsing cannot be finished with an expression so it is safe to assume there is
    # something afterward. If not thats a syntax error in the code.
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_expression(self.root_node, sym_table = symbol_table)
    #Debug(self.parser.xml)



  def test_compile_let_with_class_level_variable_lookup(self):
    symbol_table = SymbolTable()
    self.parser.class_symbol_table.define('game', 'Whatever', 'var')
    symbol_table.define('my_game', 'Whatever', 'var')
    token = Token('let', 'keyword', 1)
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('=', 'symbol', 1))
    self.tokenizer.tokens.append(Token('my_game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_let(self.root_node, token, symbol_table)
    self.assertEqual(ET.tostring(self.root_node).decode('utf-8'), '<test_root><letStatement>\n<keyword>let</keyword>\n<identifier idx="0" kind="var" usage="used">game</identifier>\n<symbol>=</symbol>\n<expression>\n<term>\n<identifier idx="0" kind="var" usage="used">my_game</identifier>\n</term>\n</expression>\n<symbol>;</symbol>\n</letStatement>\n</test_root>')

