import unittest
from tokenizer import Tokenizer

class FakeFile:
  def __init__(self):
    pass

  def tell(self):
    pass

class TokenizerTest(unittest.TestCase):
  def setUp(self):
    fake_input = FakeFile()
    self.tokenizer = Tokenizer(fake_input, 'fake_file_name')

  def test_token_order(self):
    self.tokenizer.current_line = 'if class'
    self.assertEqual(self.tokenizer.advance().value, 'if')
    self.assertEqual(self.tokenizer.advance().value, 'class')

  def test_keyword_token(self):
    self.tokenizer.current_line = 'class'
    self.assertEqual(self.tokenizer.advance().value, 'class')

  def test_symbol_and(self):
    self.tokenizer.current_line = '&'
    self.assertEqual(self.tokenizer.advance().value, '&')

  def test_symbol_lt(self):
    self.tokenizer.current_line = '<'
    self.assertEqual(self.tokenizer.advance().value, '<')

  def test_symbol_gt(self):
    self.tokenizer.current_line = '>'
    self.assertEqual(self.tokenizer.advance().value, '>')

  def test_integer_with_semicolon_token(self):
    self.tokenizer.current_line = '21216;'
    self.assertEqual(self.tokenizer.advance().value, '21216')

  def test_integer_with_space_token(self):
    self.tokenizer.current_line = '43216 '
    self.assertEqual(self.tokenizer.advance().value, '43216')

  def test_integer_with_square_bracket_token(self):
    self.tokenizer.current_line = '43216]'
    self.assertEqual(self.tokenizer.advance().value, '43216')

  def test_string_token(self):
    self.tokenizer.current_line = '"Blah blah"'
    self.assertEqual(self.tokenizer.advance().value, 'Blah blah')

  def test_string_with_space_at_beginning_token(self):
    self.tokenizer.current_line = '" Blah blah"'
    self.assertEqual(self.tokenizer.advance().value, ' Blah blah')

  def test_string_with_space_at_end_token(self):
    self.tokenizer.current_line = '" Blah blah "'
    self.assertEqual(self.tokenizer.advance().value, ' Blah blah ')

  def test_keyword_with_symbol_token(self):
    self.tokenizer.current_line = 'if('
    self.assertEqual(self.tokenizer.advance().value, 'if')
    self.assertEqual(self.tokenizer.advance().value, '(')

  def test_keyword_null(self):
    self.tokenizer.current_line = 'let c = null;'
    self.tokenizer.advance()
    self.tokenizer.advance()
    self.tokenizer.advance()
    token = self.tokenizer.advance()
    self.assertEqual(token.value, 'null')
    self.assertEqual(token.token_type, 'keyword')

  def test_identifier_token(self):
    self.tokenizer.current_line = 'test;'
    self.assertEqual(self.tokenizer.advance().value, 'test')
    self.assertEqual(self.tokenizer.advance().value, ';')

  def test_double_is_identifer_not_do_keyword_token(self):
    self.tokenizer.current_line = 'double;'
    self.assertEqual(self.tokenizer.advance().value, 'double')
    self.assertEqual(self.tokenizer.advance().value, ';')

  def test_single_line_block_token(self):
    line = '/** Single line block comment */'
    self.tokenizer._strip_comments(line)
    self.assertEqual(self.tokenizer.block_comment, False)

  def test_block_token(self):
    line = '/** Block comment starts'
    self.tokenizer._strip_comments(line)
    self.assertEqual(self.tokenizer.block_comment, True)

  def test_unkown_token(self):
    self.tokenizer.current_line = "2323aaa"
    with self.assertRaises(Exception):
      self.tokenizer.advance().value

  def test_complex_if(self):
    self.tokenizer.current_line = "if (b) {"
    self.assertEqual(self.tokenizer.advance().value, 'if')
    self.assertEqual(self.tokenizer.advance().value, '(')
    self.assertEqual(self.tokenizer.advance().value, 'b')
    self.assertEqual(self.tokenizer.advance().value, ')')
    self.assertEqual(self.tokenizer.advance().value, '{')

  def test_complex_function_def(self):
    self.tokenizer.current_line = "constructor Square new(int Ax, int Ay, int Asize) {"
    self.assertEqual(self.tokenizer.advance().value, 'constructor')
    self.assertEqual(self.tokenizer.advance().value, 'Square')
    self.assertEqual(self.tokenizer.advance().value, 'new')
    self.assertEqual(self.tokenizer.advance().value, '(')
    self.assertEqual(self.tokenizer.advance().value, 'int')
    self.assertEqual(self.tokenizer.advance().value, 'Ax')
    self.assertEqual(self.tokenizer.advance().value, ',')
    self.assertEqual(self.tokenizer.advance().value, 'int')
    self.assertEqual(self.tokenizer.advance().value, 'Ay')
    self.assertEqual(self.tokenizer.advance().value, ',')
    self.assertEqual(self.tokenizer.advance().value, 'int')
    self.assertEqual(self.tokenizer.advance().value, 'Asize')
    self.assertEqual(self.tokenizer.advance().value, ')')
    self.assertEqual(self.tokenizer.advance().value, '{')

if __name__ == '__main__':
  unittest.main()
