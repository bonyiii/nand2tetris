class VmWriter:
    def __init__(self):
        self.code = ''

    def push(self, segment, index):
        self.code += "push {} {}\n".format(segment, index)

    def pop(self, segment, index):
        self.code += "pop {} {}\n".format(segment, index)

    def arithmetic(self, command):
        self.code += "{}\n".format(command)

    def label(self, command, index):
        self.code += "label {}{}\n".format(command,index)

    def goto(self, command, index):
        self.code += "goto {}{}\n".format(command, index)

    def if_goto(self, command, index):
        self.code += "if-goto {}{}\n".format(command, index)

    def call(self, name, n_args):
        self.code += "call {} {}\n".format(name, n_args)

    def function(self, name, n_locals):
        self.code += "function {name} {n_locals}\n".format(name = name, n_locals = n_locals)

    def write_return(self, command):
        self.code += "return {}\n".format(command)

    def close(self):
        # close output file
        pass

    def save(self, output_file):
        with open(output_file, 'w') as out_file:
            out_file.write(self.code)


    def op(self, operand):
        if operand == '+':
            self.arithmetic("add")
        elif operand == "-":
            self.arithmetic("sub")
        elif operand == "*":
            self.call("Math.multiply", 2)
        elif operand == "/":
            self.call("Math.divide", 2)
        elif operand == ">":
            self.arithmetic("gt")
        elif operand == "<":
            self.arithmetic("lt")
        elif operand == "=":
            self.arithmetic("eq")
        elif operand == "&":
            self.arithmetic("and")
