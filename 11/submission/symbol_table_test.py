import unittest
from textwrap import wrap

from symbol_table import SymbolTable

if 'unittest.util' in __import__('sys').modules:
      # Show full diff in self.assertEqual.
      __import__('sys').modules['unittest.util']._MAX_LENGTH = 999999999

class SymbolTableTest(unittest.TestCase):
  def setUp(self):
    self.maxDiff = None
    self.symbol_table = SymbolTable()

  def test_define(self):
      self.symbol_table.define('x', 'int', 'field')
      self.symbol_table.define('y', 'int', 'field')
      self.symbol_table.define('pointCount', 'int', 'static')
      self.assertEqual(self.symbol_table.table['x'], { 'idx': 0, 'type_of': 'int', 'kind': 'field' } )
      self.assertEqual(self.symbol_table.table['y'], { 'idx': 1, 'type_of': 'int', 'kind': 'field' } )
      self.assertEqual(self.symbol_table.table['pointCount'], { 'idx': 0, 'type_of': 'int', 'kind': 'static' } )

  def test_define_return_value(self):
        self.symbol_table.define('x', 'int', 'field')
        res = self.symbol_table.define('y', 'int', 'field')
        self.assertEqual(res, 1)

  def test_var_count(self):
      self.symbol_table.define('x', 'int', 'field')
      self.symbol_table.define('y', 'int', 'field')
      self.symbol_table.define('pointCount', 'int', 'static')
      self.assertEqual(self.symbol_table.var_count('field'), 2)
      self.assertEqual(self.symbol_table.var_count('static'), 1)
