import xml.etree.ElementTree as ET
import os
import sys
import pdb

class Debug():
  def __init__(self, xml):
    ET(xml).write('test.xml')
    os.system('xmllint --format ./test.xml')


class ParseError(Exception):
  def __init__(self, token, expected, file_name, xml):
    self.message = "\n##################\nParseError:\n File \"{file_name}\", line {line_no} \nrecevied token: {token} is not expected. \nexpected token: {expected}\n##################\n".format(file_name=file_name, line_no=token.line_no, token=token.to_xml(), expected=expected)
    self.message += "\n"
    Debug(xml)

class UnexpectedTokenError(Exception):
  def __init__(self, expected_type, expected_value):
    self.message = "No more tokens left! \"{}\" is missing.".format(" ".join(expected_type) + " ". join(expected_value))

class Parser:
  STATEMENTS = ['let', 'if', 'while', 'do', 'return']

  def __init__(self, tokenizer):
    self.tokenizer = tokenizer
    self.xml = None
    self.term_next_token = None

  def write(self, output_file):
    tree = ET.ElementTree(self.xml)
    tree.write(output_file)

  # type
  # className
  # subroutineName
  # variableName
  # statement
  # subroutineCall

  def _build_node(self, parent_node, token):
    #print(token.to_xml())
    node = ET.SubElement(parent_node, token.token_type)
    node.text = token.value
    node.tail = "\n"

  def eat(self, parent_node, expected_value = [], expected_type = [], token = None):
    if self.term_next_token:
      token = self.term_next_token
      self.term_next_token = None
    if token or self.tokenizer.has_more_tokens():
      token = token or self.tokenizer.advance()

      #print(token.to_xml(), token.token_type, expected_type, token.token_type in expected_type)

      if (token.value in expected_value) or (token.token_type in expected_type):
        self._build_node(parent_node, token)
        return

      try:
        raise(ParseError(token, (expected_value + expected_type), self.tokenizer.file_name, self.xml))
      except ParseError as e:
        print(e.message)
      raise(Exception, "We stop here, now. If you would like to see what's in the xml so far comment out this line")

    raise(Exception, "No more tokens to eat!")


  def compile_class(self):
    parent_node = ET.Element('class')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    self.xml = parent_node
    self.eat(parent_node, expected_value=['class'])
    self.eat(parent_node, expected_type=['identifier'])
    self.eat(parent_node, expected_value=['{'])

    token = self.compile_class_var_dec(parent_node)
    token = self.compile_subroutine_dec(parent_node, token)

    self.eat(parent_node, expected_value=['}'], token = token)
    return parent_node


  def compile_class_var_dec(self, grandparent_node):
    while True:
      if self.tokenizer.has_more_tokens():
        token = self.tokenizer.advance()
        if (token.value in ['static', 'field']):
          parent_node = ET.SubElement(grandparent_node, 'classVarDec')
          parent_node.text = "\n"
          parent_node.tail = "\n"
          # static | field
          self._build_node(parent_node, token)
          # type
          self.eat(parent_node, expected_value = ['int', 'char', 'boolean'], expected_type = ['identifier'])
          # varName*
          self._one_or_more_identifier(parent_node)
        else:
          return token
      else:
        break


  def compile_subroutine_dec(self, grandparent_node, token):
    while True:
      if (token.value in ['constructor', 'function', 'method']):
          parent_node = ET.SubElement(grandparent_node, 'subroutineDec')
          parent_node.text = "\n"
          parent_node.tail = "\n"
          # constructor | function | method
          self._build_node(parent_node, token)

          if token.value == 'constructor':
            # new
            self.eat(parent_node, expected_type = ['identifier'])
          else:
            # void | type
            self.eat(parent_node, expected_value = ['int', 'char', 'boolean', 'void'], expected_type = ['identifier'])
          # subroutineName
          self.eat(parent_node, expected_type = ['identifier'])

          self.eat(parent_node, expected_value = ['('])
          # parameterList
          token = self.compile_parameter_list(parent_node)
          self.eat(parent_node, expected_value=[')'], token = token)

          self.compile_subroutine_body(parent_node)
          token = self.tokenizer.next_token()
      else:
          return token


  def _one_or_more_identifier(self, parent_node):
    while True:
      self.eat(parent_node, expected_type=['identifier'])
      token = self.tokenizer.next_token()
      if token.value in [',']:
        self.eat(parent_node, expected_value=[','], token = token)
      else:
        self.eat(parent_node, expected_value=[';'], token = token)
        break


  def compile_parameter_list(self, grandparent_node):
    parent_node = ET.SubElement(grandparent_node, 'parameterList')
    parent_node.text = "\n"
    parent_node.tail = "\n"

    while True:
      token = self.tokenizer.next_token()
      if token.value == ')':
        parent_node.text = "\n"
        return token

      self.eat(parent_node, expected_type=['keyword'], token = token) # type
      self.eat(parent_node, expected_type=['identifier']) # name

      token = self.tokenizer.next_token()
      if token.value in [',']:
        self._build_node(parent_node, token)
      else:
        return token


  def compile_subroutine_body(self, grandparent_node):
    parent_node = ET.SubElement(grandparent_node, 'subroutineBody')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    self.eat(parent_node, expected_value = ['{'])

    while True:
      if self.tokenizer.has_more_tokens():
        token = self.tokenizer.advance()
        if token.value in ['var']:
          self.compile_var_dec(parent_node, token)
        elif token.value in self.STATEMENTS:
          token = self.compile_statements(parent_node, token)
          break
        else:
          break

    self.eat(parent_node, expected_value = ['}'], token = token)


  def compile_var_dec(self, grandparent_node, token):
    parent_node = ET.SubElement(grandparent_node, 'varDec')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    # var
    self.eat(parent_node, expected_value = ['var'], token = token)
    # type
    self.eat(parent_node, expected_value = ['int', 'char', 'boolean'], expected_type = ['identifier'])
    # varName*
    self._one_or_more_identifier(parent_node)


  def compile_statements(self, grandparent_node, token):
    parent_node = ET.SubElement(grandparent_node, 'statements')
    parent_node.text = "\n"
    parent_node.tail = "\n"

    while True:
      if token.value == 'let':
        self.compile_let(parent_node, token)
      elif token.value == 'if':
        if_node = self.compile_if(parent_node, token)
      elif token.value == 'else':
        # an "else" node must be connected to an "if" node
        self.compile_else(if_node, token)
      elif token.value == 'do':
        self.compile_do(parent_node, token)
      elif token.value == 'return':
        self.compile_return(parent_node, token)
      elif token.value == 'while':
        self.compile_while(parent_node, token)
      else:
        return token

      if self.tokenizer.has_more_tokens():
        token = self.tokenizer.advance()


  def compile_let(self, grandparent_node, token):
    parent_node = ET.SubElement(grandparent_node, 'letStatement')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    self.eat(parent_node, expected_value = ['let'], token = token)
    self.eat(parent_node, expected_type = ['identifier'])

    token = self.tokenizer.next_token()
    if token.value == '[':
      self.eat(parent_node, expected_value = ['['], token = token)
      token = self.compile_expression(parent_node)
      self.eat(parent_node, expected_value = [']'], token = token)
      self.eat(parent_node, expected_value = ['='])
    else:
      self.eat(parent_node, expected_value = ['='], token = token)

    token = self.compile_expression(parent_node)
    self.eat(parent_node, expected_value = [';'], token = token)


  def compile_if(self, grandparent_node, token):
    parent_node = ET.SubElement(grandparent_node, 'ifStatement')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    self.eat(parent_node, expected_value = ['if'], token = token)
    self.eat(parent_node, expected_value = ['('])
    token = self.compile_expression(parent_node)
    self.eat(parent_node, expected_value = [')'], token = token)

    # if body
    self.eat(parent_node, expected_value = ['{'])
    token = self.compile_statements(parent_node, self.tokenizer.next_token())
    self.eat(parent_node, expected_value = ['}'], token=token)
    return parent_node


  def compile_else(self, parent_node, token):
    self.eat(parent_node, expected_value = ['else'], token=token)

    # else body
    self.eat(parent_node, expected_value = ['{'])
    token = self.compile_statements(parent_node, self.tokenizer.next_token())
    self.eat(parent_node, expected_value = ['}'], token=token)


  def compile_while(self, grandparent_node, token):
    parent_node = ET.SubElement(grandparent_node, 'whileStatement')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    self.eat(parent_node, expected_value = ['while'], token = token)
    self.eat(parent_node, expected_value = ['('])
    token = self.compile_expression(parent_node)
    self.eat(parent_node, expected_value = [')'], token = token)

    # while body
    self.eat(parent_node, expected_value = ['{'])
    token = self.compile_statements(parent_node, self.tokenizer.next_token())
    self.eat(parent_node, expected_value = ['}'], token=token)


  def compile_do(self, grandparent_node, token):
    parent_node = ET.SubElement(grandparent_node, 'doStatement')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    self.eat(parent_node, expected_value = ['do'], token = token)
    self.subroutine_call(parent_node)
    self.eat(parent_node, expected_value = [';'])


  def compile_return(self, grandparent_node, token):
    parent_node = ET.SubElement(grandparent_node, 'returnStatement')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    self.eat(parent_node, expected_value = ['return'], token = token)

    token = self.tokenizer.next_token()
    if token.value == ';':
      self.eat(parent_node, expected_value = [';'], token = token)
    else:
      token = self.compile_expression(parent_node, token = token)
      self.eat(parent_node, expected_value = [';'], token = token)


  def compile_expression_list(self, grandparent_node):
    parent_node = ET.SubElement(grandparent_node, 'expressionList')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    next_token = None

    while True:
      token = next_token or self.tokenizer.next_token()
      next_token = None

      if token.value == ")":
        return token
      elif token.value == ",":
        self.eat(parent_node, expected_value=[','], token = token)
      else:
        next_token = self.compile_expression(parent_node, token)


  def compile_expression(self, grandparent_node, token = None):
    parent_node = ET.SubElement(grandparent_node, 'expression')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    token = token or self.tokenizer.next_token()
    term_next_token = self.compile_term(parent_node, token)

    while True:
      term_next_token = term_next_token or self.tokenizer.next_token()
      # op
      if term_next_token.value in ['+', '-', '*', '/', '&', '|', '<', '>', '=']:
        # eats term_next_token
        self.eat(parent_node, expected_type = ['symbol'], token = term_next_token)
        # term
        term_next_token = self.compile_term(parent_node, self.tokenizer.next_token())
      else:
        return term_next_token


  def compile_term(self, grandparent_node, token):
    parent_node = ET.SubElement(grandparent_node, 'term')
    parent_node.text = "\n"
    parent_node.tail = "\n"
    term_next_token = self.tokenizer.next_token()

    # varName[expression]
    if term_next_token.value in ['[']:
      self.eat(parent_node, expected_type = ['identifier', 'keyword'], token = token)
      # eats term_next_node
      self.eat(parent_node, expected_value = ['['], token = term_next_token)
      token = self.compile_expression(parent_node)
      self.eat(parent_node, expected_value = [']'], token = token)
    # (expression)
    elif token.value == '(':
      self.eat(parent_node, expected_value = ['('], token = token)
      # eats term_next_node
      token = self.compile_expression(parent_node, token = term_next_token)
      self.eat(parent_node, expected_value = [')'], token = token)
      # unaryOp term
    elif token.value in ['-', '~']:
      self.eat(parent_node, expected_type = ['symbol'], token = token)
      return self.compile_term(parent_node, token = term_next_token)

    # subroutineCall
    elif term_next_token.value in ['(', '.']:
      self.subroutine_call(parent_node, term_token = token, term_next_token = term_next_token)
    # varName or constants
    else:
      self.eat(parent_node, expected_type = ['integerConstant', 'stringConstant', 'keyword', 'identifier'], token = token)
      return term_next_token


  def subroutine_call(self, parent_node, term_token = None, term_next_token = None):
    if term_token:
      self.eat(parent_node, expected_type = ['identifier'], token = term_token)
      token = term_next_token
    else:
      self.eat(parent_node, expected_type = ['identifier'])
      token = self.tokenizer.next_token()

    if token.value == '(':
      self.eat(parent_node, expected_value = ['('], token = token)
    elif token.value == '.':
      self.eat(parent_node, expected_value = ['.'], token = token)
      self.eat(parent_node, expected_type = ['identifier'])
      self.eat(parent_node, expected_value = ['('])

    token = self.compile_expression_list(parent_node)
    self.eat(parent_node, expected_value = [')'], token = token)
