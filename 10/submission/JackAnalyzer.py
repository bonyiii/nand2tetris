#!/usr/bin/env python3

import os
import glob
import argparse
import xml.etree.ElementTree as ET
from tokenizer import Tokenizer
from parser import Parser

parser = argparse.ArgumentParser(description='Process .')
parser.add_argument('input_file', help='name of the input file')
#parser.add_argument('output_file', help='name of the output file')
args = parser.parse_args()

input_files = []

if os.path.isdir(args.input_file):
  dir_path = args.input_file
  if dir_path[-1] == '/':
    dir_path = dir_path[:-1]
  for input_file in glob.glob('{}/*.jack'.format(dir_path)):
    input_files.append(input_file)

elif os.path.isfile(args.input_file):
  input_files.append(args.input_file)

for file_name in input_files:
  with open(file_name, 'r') as in_file:
    tokenizer = Tokenizer(in_file, file_name)
    parser = Parser(tokenizer)
    parser.compile_class()
    output_file =file_name.replace('.jack', '.xml')
    print(output_file)
    parser.write(output_file)
    #os.system('xmllint --format ./test.xml | tail -n +2 > {}'.format(output_file))
    # while tokenizer.has_more_tokens():
    #   token = tokenizer.advance()
    #   print(token.to_xml())
