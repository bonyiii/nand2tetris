import re
import pdb
import time

class Token:
  def __init__(self, value, token_type, line_no):
    self.value = value
    self.token_type = token_type
    self.line_no = line_no

  def to_xml(self):
    return "<{type}> {token} </{type}>".format(token=self.value,type=self.token_type)

class Tokenizer:
  KEYWORDS = ["class", "constructor", "function", "method", "field", "static", "var", "int", "char", "boolean", "void", "true", "false",
             "null", "this", "let", "do", "if", "else", "while", "return"]
  SYMBOLS = ["{", "}", "(", ")", "[", "]", ".", ",", ";", "+", "-", "*", "/", "&", "|", "<", ">", "=", "~"]
  INT = re.compile(r'\d+(?!\w)')
  STR = re.compile(r'".+"')
  IDENTIFIER = re.compile(r'(?<!\d)[a-zA-Z_]+')

  def __init__(self, in_file, file_name):
    self.in_file = in_file
    self.file_name = file_name
    self.position = self.in_file.tell()
    self.end_of_file_reached = False
    self.block_comment = False
    self.current_line = ''
    self.line_no = 0

  def next_token(self):
    if self.has_more_tokens():
      return self.advance()

  def has_more_tokens(self):
    """
    If current line is empty, reads the next line,
      then if the end of the file hasn't reached, calls itself recursively.

    If (regardles whether the line is empty or not) the end of file is reached
    return False (indication no more tokens are available) otherwise True.
    """
    #print(self.line_no, self.current_line)
    #time.sleep(0.1)
    if self.current_line == '':
      self.current_line = self._readline()
      self.line_no += 1

      if not self.end_of_file_reached:
        self.has_more_tokens()

    return not self.end_of_file_reached

  def advance(self):
    """
    Returns the current token, it should be called only if has_more_tokens returns true

    """
    # Check for strings
    token = re.match(self.STR, self.current_line)
    if token:
      self.current_line = self.current_line.replace(token.group(0), '', 1).strip()
      return Token(token.group(0)[1:-1], 'stringConstant', self.line_no)

    # Check for integer
    token = re.match(self.INT, self.current_line)
    if token:
      self.current_line = self.current_line.replace(token.group(0), '', 1).strip()
      return Token(token.group(0), 'integerConstant', self.line_no)

    # Check for keyword
    token = re.match("|".join(self.KEYWORDS), self.current_line)
    if token:
      self.current_line = self.current_line.replace(token.group(0), '', 1).strip()
      return Token(token.group(0), 'keyword', self.line_no)

    # Check for symbol
    symbols = "|".join([re.escape(symbol) for symbol in self.SYMBOLS])
    token = re.match(symbols, self.current_line)

    if token:
      self.current_line = self.current_line.replace(token.group(0), '', 1).strip()
      token_value = token.group(0)
      return Token(token_value, 'symbol', self.line_no)

    # Check for identifier
    token = re.match(self.IDENTIFIER, self.current_line)
    if token:
      self.current_line = self.current_line.replace(token.group(0), '', 1).strip()
      return Token(token.group(0), 'identifier', self.line_no)

    raise Exception("Tokinzer#advance(): Unknonw token!")

  def _readline(self):
    current_line = self.in_file.readline()
    self.position = self.in_file.tell()
    self.in_file.seek(self.position)

    if current_line == "":
      self.end_of_file_reached = True

    return self._strip_comments(current_line).strip()

  def _strip_comments(self, line):
    """
    Strip single and multi line of comments.
    """
    block_comment_start = re.compile(r'\/\*\*.*')
    block_comment_end = re.compile(r'.*\*\/')
    block_comment = False
    """
    Mark if a block comment starts
    if there are code before the start of the block commit,
    even though it is ugly we have to accept it as valid eg:

    var x = 11; /** This is the beginning of a block comment
                    which end here */

    The tokens before the /** must be recognized
    """
    if re.search(block_comment_start, line):
      # Single line block comment
      if line.strip()[-2:] == '*/':
        return ''
      block_comment = True
      line = re.sub(block_comment_start, '', line)
    if re.search(block_comment_end, line):
      self.block_comment = False
      line = re.sub(block_comment_end, '', line)

    if self.block_comment:
      return ''
    else:
      if block_comment:
        self.block_comment = True
      return line.strip().split('//')[0]
