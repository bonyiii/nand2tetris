import unittest
import xml.etree.ElementTree as ET
import pdb

from parser import Parser, Debug
from tokenizer import Token

class Tokenizer:
  def __init__(self):
    self.tokens = []
    self.file_name = 'FakeFile'

  def has_more_tokens(self):
    return len(self.tokens) > 0

  def advance(self):
    return self.tokens.pop()

  def next_token(self):
    if self.has_more_tokens():
      return self.advance()


class ParserTest(unittest.TestCase):
  def setUp(self):
    self.tokenizer = Tokenizer()
    self.root_node = ET.Element('test_root')
    self.parser = Parser(self.tokenizer)
    self.parser.xml = self.root_node

  def test_compile_class_var_dec(self):
    self.tokenizer.tokens.append(Token('field', 'keyword', 1))
    self.tokenizer.tokens.append(Token('int', 'keyword', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    closing_curly_token = Token('}', 'symbol', 1)
    self.tokenizer.tokens.append(closing_curly_token)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_class_var_dec(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><classVarDec><keyword>field</keyword><keyword>int</keyword><identifier>x</identifier><symbol>,</symbol><identifier>y</identifier><symbol>;</symbol></classVarDec></test_root>')
    self.assertEqual(result, closing_curly_token)

  def test_one_or_more_identifier(self):
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser._one_or_more_identifier(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><identifier>x</identifier><symbol>,</symbol><identifier>y</identifier><symbol>;</symbol></test_root>')

  def test_compile_subroutine_dec(self):
    token = Token('constructor', 'keyword', 1)
    self.tokenizer.tokens.append(Token('Square', 'identifier', 1))
    self.tokenizer.tokens.append(Token('new', 'identifier', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('int', 'keyword', 1))
    self.tokenizer.tokens.append(Token('Ax', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_subroutine_dec(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><subroutineDec><keyword>constructor</keyword><identifier>Square</identifier><identifier>new</identifier><symbol>(</symbol><parameterList><keyword>int</keyword><identifier>Ax</identifier></parameterList><symbol>)</symbol><subroutineBody><symbol>{</symbol><symbol>}</symbol></subroutineBody></subroutineDec></test_root>')

  def test_compile_parameter_list_empty(self):
    closing_parent = Token(')', 'symbol', 1)
    self.tokenizer.tokens.append(closing_parent)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_parameter_list(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><parameterList>\n</parameterList></test_root>')
    self.assertEqual(result, closing_parent)

  def test_compile_parameter_list(self):
    self.tokenizer.tokens.append(Token('int', 'keyword', 1))
    self.tokenizer.tokens.append(Token('Ax', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('int', 'keyword', 1))
    self.tokenizer.tokens.append(Token('Ay', 'identifier', 1))
    closing_parent = Token(')', 'symbol', 1)
    self.tokenizer.tokens.append(closing_parent)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_parameter_list(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><parameterList><keyword>int</keyword><identifier>Ax</identifier><symbol>,</symbol><keyword>int</keyword><identifier>Ay</identifier></parameterList></test_root>')
    self.assertEqual(result, closing_parent)

  def test_compile_subroutine_body(self):
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_subroutine_body(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><subroutineBody><symbol>{</symbol><symbol>}</symbol></subroutineBody></test_root>')

  def test_compile_subroutine_body_with_var_dec(self):
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('var', 'keyword', 1))
    self.tokenizer.tokens.append(Token('SquareGame', 'identifier', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_subroutine_body(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><subroutineBody><symbol>{</symbol><varDec><keyword>var</keyword><identifier>SquareGame</identifier><identifier>x</identifier><symbol>,</symbol><identifier>y</identifier><symbol>;</symbol></varDec><symbol>}</symbol></subroutineBody></test_root>')

  def test_compile_subroutine_body_with_statements(self):
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('return', 'keyword', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_subroutine_body(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><subroutineBody><symbol>{</symbol><statements>\n<returnStatement><keyword>return</keyword><symbol>;</symbol></returnStatement></statements><symbol>}</symbol></subroutineBody></test_root>')

  def test_compile_var_dec(self):
    token = Token('var', 'keyword', 1)
    self.tokenizer.tokens.append(Token('SquareGame', 'identifier', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_var_dec(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><varDec><keyword>var</keyword><identifier>SquareGame</identifier><identifier>x</identifier><symbol>,</symbol><identifier>y</identifier><symbol>;</symbol></varDec></test_root>')

  def test_compile_let(self):
    token = Token('let', 'keyword', 1)
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('=', 'symbol', 1))
    # TODO exression
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_let(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><letStatement><keyword>let</keyword><identifier>game</identifier><symbol>=</symbol><expression><term><identifier>game</identifier></term></expression><symbol>;</symbol></letStatement></test_root>')

  def test_compile_do(self):
    token = Token('do', 'keyword', 1)
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('.', 'symbol', 1))
    self.tokenizer.tokens.append(Token('run', 'identifier', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_do(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><doStatement><keyword>do</keyword><identifier>game</identifier><symbol>.</symbol><identifier>run</identifier><symbol>(</symbol><expressionList>\n</expressionList><symbol>)</symbol><symbol>;</symbol></doStatement></test_root>')

  # do game.run(this);
  def test_compile_do_with_keyword(self):
    token = Token('do', 'keyword', 1)
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('.', 'symbol', 1))
    self.tokenizer.tokens.append(Token('run', 'identifier', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('this', 'keyword', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_do(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><doStatement><keyword>do</keyword><identifier>game</identifier><symbol>.</symbol><identifier>run</identifier><symbol>(</symbol><expressionList>\n<expression><term><keyword>this</keyword></term></expression></expressionList><symbol>)</symbol><symbol>;</symbol></doStatement></test_root>')

  def test_compile_if(self):
    token = Token('if', 'keyword', 1)
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    # There must be something that wraps the if statement (like function body), so assuming there is a closing curly brace is safe
    # so this brace is only here to emulate that
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_if(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><ifStatement><keyword>if</keyword><symbol>(</symbol><expression><term><identifier>game</identifier></term></expression><symbol>)</symbol><symbol>{</symbol><statements>\n</statements><symbol>}</symbol></ifStatement></test_root>')

  def test_compile_nested_if_else(self):
    token = Token('if', 'keyword', 1)
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))

    # if/else within an if
    self.tokenizer.tokens.append(Token('if', 'keyword', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('nested', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens.append(Token('else', 'keyword', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))

    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_if(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><ifStatement><keyword>if</keyword><symbol>(</symbol><expression><term><identifier>game</identifier></term></expression><symbol>)</symbol><symbol>{</symbol><statements>\n<ifStatement><keyword>if</keyword><symbol>(</symbol><expression><term><identifier>nested</identifier></term></expression><symbol>)</symbol><symbol>{</symbol><statements>\n</statements><symbol>}</symbol><keyword>else</keyword><symbol>{</symbol><statements>\n</statements><symbol>}</symbol></ifStatement></statements><symbol>}</symbol></ifStatement></test_root>')

  def test_compile_else(self):
    token = Token('else', 'keyword', 1)
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_else(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><keyword>else</keyword><symbol>{</symbol><statements>\n</statements><symbol>}</symbol></test_root>')

  def test_compile_else_with_statements(self):
    token = Token('else', 'keyword', 1)
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('return', 'keyword', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_else(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><keyword>else</keyword><symbol>{</symbol><statements>\n<returnStatement><keyword>return</keyword><symbol>;</symbol></returnStatement></statements><symbol>}</symbol></test_root>')

  def test_compile_while(self):
    token = Token('while', 'keyword', 1)
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_while(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><whileStatement><keyword>while</keyword><symbol>(</symbol><expression><term><identifier>game</identifier></term></expression><symbol>)</symbol><symbol>{</symbol><statements>\n</statements><symbol>}</symbol></whileStatement></test_root>')

  def test_compile_subroutine_call(self):
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token('.', 'symbol', 1))
    # TODO exression
    self.tokenizer.tokens.append(Token('run', 'identifier', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.subroutine_call(self.root_node)
    #ET.dump(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><identifier>game</identifier><symbol>.</symbol><identifier>run</identifier><symbol>(</symbol><expressionList>\n</expressionList><symbol>)</symbol></test_root>')

  def test_compile_expression_list(self):
    closing_parent = Token(')', 'symbol', 1)
    self.tokenizer.tokens.append(closing_parent)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_expression_list(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><expressionList>\n</expressionList></test_root>')
    self.assertEqual(result, closing_parent)

  # (good, game)
  # the opening parenthesis parsed earlier
  def test_compile_expression_list_with_multiple_expressions(self):
    closing_parent = Token(')', 'symbol', 1)
    self.tokenizer.tokens.append(Token('good', 'identifier', 1))
    self.tokenizer.tokens.append(Token(',', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(closing_parent)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    result = self.parser.compile_expression_list(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><expressionList>\n<expression><term><identifier>good</identifier></term></expression><symbol>,</symbol><expression><term><identifier>game</identifier></term></expression></expressionList></test_root>')
    self.assertEqual(result, closing_parent)

  def test_compile_return_with_var(self):
    token = Token('return', 'keyword', 1)
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_return(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><returnStatement><keyword>return</keyword><expression><term><identifier>game</identifier></term></expression><symbol>;</symbol></returnStatement></test_root>')

  def test_compile_plain_return(self):
    token = Token('return', 'keyword', 1)
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_return(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><returnStatement><keyword>return</keyword><symbol>;</symbol></returnStatement></test_root>')


  def test_compile_term_string(self):
    token = Token('good thing', 'stringConstant', 1)
    semicolon_token = Token(';', 'symbol', 1)
    self.tokenizer.tokens.append(semicolon_token)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_term(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><term><stringConstant>good thing</stringConstant></term></test_root>')

  def test_compile_term_integer(self):
    token = Token('27', 'integerConstant', 1)
    semicolon_token = Token(';', 'symbol', 1)
    self.tokenizer.tokens.append(semicolon_token)
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_term(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><term><integerConstant>27</integerConstant></term></test_root>')

  def test_compile_term_array(self):
    token = Token('a', 'identifier', 1)
    self.tokenizer.tokens.append(Token('[', 'symbol', 1))
    self.tokenizer.tokens.append(Token('2', 'integerConstant', 1))
    self.tokenizer.tokens.append(Token(']', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_term(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><term><identifier>a</identifier><symbol>[</symbol><expression><term><integerConstant>2</integerConstant></term></expression><symbol>]</symbol></term></test_root>')

  # i = i * (-j);
  def test_compile_expression(self):
    self.tokenizer.tokens.append(Token('i', 'identifier', 1))
    self.tokenizer.tokens.append(Token('=', 'symbol', 1))
    self.tokenizer.tokens.append(Token('i', 'identifier', 1))
    self.tokenizer.tokens.append(Token('*', 'symbol', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('-', 'symbol', 1))
    self.tokenizer.tokens.append(Token('j', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token(';', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_expression(self.root_node)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><expression><term><identifier>i</identifier></term><symbol>=</symbol><term><identifier>i</identifier></term><symbol>*</symbol><term><symbol>(</symbol><expression><term><symbol>-</symbol><term><identifier>j</identifier></term></term></expression><symbol>)</symbol></term></expression></test_root>')

    #while (~exit) {
  def test_compile_while_with_unary_expression(self):
    token = Token('while', 'keyword', 1)
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('~', 'symbol', 1))
    self.tokenizer.tokens.append(Token('game', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('{', 'symbol', 1))
    self.tokenizer.tokens.append(Token('}', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_while(self.root_node, token)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><whileStatement><keyword>while</keyword><symbol>(</symbol><expression><term><symbol>~</symbol><term><identifier>game</identifier></term></term></expression><symbol>)</symbol><symbol>{</symbol><statements>\n</statements><symbol>}</symbol></whileStatement></test_root>')

    # if (((y + size) < 254) & ((x + size) < 510)) {
    # This is the expression part: ((y + size) < 254) & ((x + size) < 510)
    # expression(term op term)
  def test_compile_expression_nested(self):
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('y', 'identifier', 1))
    self.tokenizer.tokens.append(Token('+', 'symbol', 1))
    self.tokenizer.tokens.append(Token('size', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('&lt;', 'symbol', 1))
    self.tokenizer.tokens.append(Token('254', 'integerConstant', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('&amp;', 'symbol', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('(', 'symbol', 1))
    self.tokenizer.tokens.append(Token('x', 'identifier', 1))
    self.tokenizer.tokens.append(Token('+', 'symbol', 1))
    self.tokenizer.tokens.append(Token('size', 'identifier', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens.append(Token('&lt;', 'symbol', 1))
    self.tokenizer.tokens.append(Token('510', 'integerConstant', 1))
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    # Last closing parent for the if ) this is not going to be processed by the expression
    # but parsing cannot be finished with an expression so it is safe to assume there is
    # something afterward. If not thats a syntax error in the code.
    self.tokenizer.tokens.append(Token(')', 'symbol', 1))
    self.tokenizer.tokens = self.tokenizer.tokens[::-1]

    self.parser.compile_expression(self.root_node)
    #Debug(self.parser.xml)
    self.assertEqual(ET.tostring(self.root_node), '<test_root><expression><term><symbol>(</symbol><expression><term><symbol>(</symbol><expression><term><identifier>y</identifier></term><symbol>+</symbol><term><identifier>size</identifier></term></expression><symbol>)</symbol></term><symbol>&amp;lt;</symbol><term><integerConstant>254</integerConstant></term></expression><symbol>)</symbol></term><symbol>&amp;amp;</symbol><term><symbol>(</symbol><expression><term><symbol>(</symbol><expression><term><identifier>x</identifier></term><symbol>+</symbol><term><identifier>size</identifier></term></expression><symbol>)</symbol></term><symbol>&amp;lt;</symbol><term><integerConstant>510</integerConstant></term></expression><symbol>)</symbol></term></expression></test_root>')


