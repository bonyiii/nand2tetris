// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/12/String.jack

/**
 * Represents character strings. In addition for constructing and disposing
 * strings, the class features methods for getting and setting individual
 * characters of the string, for erasing the string's last character,
 * for appending a character to the string's end, and more typical
 * string-oriented operations.
 */
class String {
    field Array str;
    field int len;

    /** constructs a new empty string with a maximum length of maxLength
     *  and initial length of 0. */
    constructor String new(int maxLength) {
        let str = Array.new(maxLength+1);
        let len = 0;
        return this;
    }

    /** Disposes this string. */
    method void dispose() {
        do Memory.deAlloc(str);
        return;
    }

    /** Returns the current length of this string. */
    method int length() {
        return len;
    }

    /** Returns the character at the j-th location of this string. */
    method char charAt(int j) {
        return str[j];
    }

    /** Sets the character at the j-th location of this string to c. */
    method void setCharAt(int j, char c) {
        let str[j] = c;
        return;
    }

    /** Appends c to this string's end and returns this string. */
    method String appendChar(char c) {
        let str[len] = c;
        let len = len + 1;
        return this;
    }

    /** Erases the last character from this string. */
    method void eraseLastChar() {
        let len = len - 1;
        return;
    }

    /** Returns the integer value of this string, 
     *  until a non-digit character is detected. */
    method int intValue() {
        var int val, i, digit;
        let val = 0;
        let i = 0;
        while(i < len) {
            let digit = charAt(i) - 48;
            let val = val * 10 + digit;
            let i = i + 1;
        }
        return val;
    }

    /** Sets this string to hold a representation of the given value. */
    method void setInt(int val) {
        var int last_digit;
        var char c;
        let last_digit = String.mod(val, 10);
        let c = String.digit(last_digit);
        do appendChar(c);

        // val >= 10
        if (~(val < 10)) {
            do setInt(val / 10);
        }
        return;
    }

    function int mod(int x, int y) {
        return (x - (y * (x / y)));
    }


    function char digit(int digit) {
        return 48 + digit;
    }

    /** Returns the new line character. */
    function char newLine() {
        return 128;
    }

    /** Returns the backspace character. */
    function char backSpace() {
        return 129;
    }

    /** Returns the double quote (") character. */
    function char doubleQuote() {
        return 34;
    }
}
