from parser import Parser

class CodeWriter:
  def __init__(self, static_namespace):
    self.lines = []

    self.static_namespace = static_namespace
    self.arithmetic = Arithmetic(stack = self)
    self.memory_segment = MemorySegment(stack = self)

  def write(self, parser):
    self.lines.append("// {}".format(parser.current_line()))

    #print(parser.current_command, parser.current_line())
    if parser.command_type() == Parser.C_PUSH:
      self.memory_segment.push(parser.arg1(), parser.arg2())
    elif parser.command_type() == Parser.C_POP:
      self.memory_segment.pop(parser.arg1(), parser.arg2())
    if parser.command_type() == Parser.C_ARITHMETIC:
      self.arithmetic.run(parser.command())

  def push(self):
    # Go to stack pointer location and by that select RAM memory register
    self.lines.append("@SP")
    # Set address to the value stored at the RAM memory register, eg: *SP (the value where the stack pointer points to)
    self.lines.append("A=M")
    # Save CPU data register value into the RAM memory register
    self.lines.append("M=D")
    # Go to the stack pointer location
    self.lines.append("@SP")
    # Increment stack pointer value
    # the @SP location is fixed in the memory, its value points to the next available space
    # so that's why we update the register value
    self.lines.append("M=M+1")

  def pop(self):
    """
    Set A register to @SP value, so next time when M is read, it will return @SP value (eg: Go to memory segment where @SP points)

    Set A to value - 1, eg: jump to value-1, so next time M at that address will be read
    Set M to value - 1, eg: decrement SP value to point to this new address
    """

    # Go to the stack pointer location
    self.lines.append("@SP")
    # Decrement the stack pointer value (eg: the @SP register value gets decremented by 1),
    # and jumps to the (old value - 1) of the @SP register,
    self.lines.append("AM=M-1")

  def get(self):
    """
    Set A register to @SP so next time when M is read it will return the @SP value (Go to memory segment where @SP points)

    Set A to value - 1, eg: jump to that value, this way next time we read M value from that register will be read)
    Don't change SP, so next push will overwrite this M memory segment's value
    """

    # Go to the stack pointer location
    self.lines.append("@SP")
    # Leave stack pointer value unchanged, eg: don't overwrite RAM[0] (or whatever @SP is set to) value
    # and jumps to the @SP register value - 1
    self.lines.append("A=M-1")


class MemorySegment:
  def __init__(self, *, stack):
    self.lines = stack.lines
    self.stack = stack
    self.segments = {
      'local': "LCL",
      'argument': "ARG",
      'this': "THIS",
      'that': "THAT",
      'temp': 5,
      'pointer': 3,
      'static': 16
    }

  def pop(self, segment, index):
    if segment == 'constant':
      self._save_constant(index)
    else:
      fixed_address = False
      if segment in ['temp', 'pointer', 'static']:
        fixed_address = True
      self._save_segment(self.segments[segment], index, fixed_address)

  def _save_constant(self, index):
    self.stack.pop()
    # Set A register to the value of the selected memory segment and index
    self.lines.append("@{}".format(index))
    # Load CPU D register value into RAM M memory segment
    self.lines.append("M=D")

  def _save_segment(self, segment, index, fixed_address):
    # Goto memory segment base address
    self.lines.append("@{}".format(segment))

    if fixed_address:
      # Load address into CPU D register (temp is fixed at address 3)
      self.lines.append("D=A")
    else:
      # Load A(address) value stored in memory segment (M) into CPU D register, eg: (*SEGMENT)
      self.lines.append("D=M")

    # Goto index
    self.lines.append("@{}".format(index))
    # Write index + segment base adress into CPU D(ata) register
    self.lines.append("D=D+A")
    # Goto R13 general purpose register
    self.lines.append("@R13")
    # Save segment + index address in @R13
    self.lines.append("M=D")

    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")

    # Goto R13 general purpose register, read its value and jump there (eg: *R13)
    self.lines.append("@R13")
    self.lines.append("A=M")

    # Load CPU D register value into memory M register
    self.lines.append("M=D")


  def push(self, segment, index):
    if segment == 'constant':
      self._load_constant(index)
    else:
      fixed_address = False
      if segment in ['temp', 'pointer', 'static']:
        fixed_address = True
      self._load_segment(self.segments[segment], index, fixed_address)

  def _load_segment(self, segment, index, fixed_address):
    # Go to index
    self.lines.append("@{}".format(index))
    # Load index address into CPU D register
    self.lines.append("D=A")
    # Go to this
    self.lines.append("@{}".format(segment))

    if fixed_address:
      # Set address to: index + address
      self.lines.append("A=D+A")
    else:
      # Set address to: index + *this (eg: offset *this (value) by the index value)
      self.lines.append("A=D+M")

    # Load memory register M value into CPU D register
    self.lines.append("D=M")
    self.stack.push()

  def _load_constant(self, index):
    # Set A register to the value of the selected memory segment and index
    self.lines.append("@{}".format(index))
    # Load address into CPU data register
    self.lines.append("D=A")
    self.stack.push()


class Arithmetic:
  def __init__(self, *, stack):
    self.lines = stack.lines
    self.stack = stack

    self.label_idx = 0

  def run(self, command):
    operation = {
      'neg': self._neg,
      'not': self._not,
      'add': self.__add,
      'sub': self.__sub,
      'eq': self.__eq,
      'lt': self.__lt,
      'gt': self.__gt,
      'and': self.__and,
      'or': self.__or
    }[command]

    if command in ['add', 'sub']:
      self._arithmetic(operation)
    elif command in ['and', 'or']:
      self._logical(operation)
    elif command in [ 'eq', 'lt', 'gt']:
      self._comparison(operation)
    else:
      operation()

  def __and(self):
    self.lines.append("M=D&M")

  def __or(self):
    self.lines.append("M=D|M")

  def _logical(self, func):
    # Get first value
    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")
    self.stack.get()

    # logical operation
    func()

  def __eq(self):
    self.lines.append("D;JEQ")

  def __lt(self):
    self.lines.append("D;JLT")

  def __gt(self):
    self.lines.append("D;JGT")

  def _comparison(self, func):
    # Get first value
    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")
    self.stack.get()

    # Set CPU D register equal to the difference of the: second value - first value
    # later, in the func() this value can be compared whether it is greater, equal or less then 0
    self.lines.append("D=M-D")
    # Assume the condition will evaluated to true
    self.lines.append("M=-1")
    # Set target label address to jump to if condition evaluated true
    self.lines.append("@logical.{}".format(self.label_idx) )

    # comparison
    func()

    # if these lines aren't skipped using the label,
    # that means the condition evaluated to false
    # Read stack pointer value
    self.lines.append("@SP")
    # Go to its target + 1 becuause pop moved the pointer 1 further down
    self.lines.append("A=M-1")
    # Set memory register to equal to false
    self.lines.append("M=0")

    # Create a label to jump to if the condition evaluated true,
    # even though this line is later in the file, it will be parsed by the
    # assembler in the first pass, while the rest of the code will be parsed
    # in the second pass
    self.lines.append("(logical.{})".format(self.label_idx))
    self.label_idx += 1

  def __add(self):
    # Add the Data register and the memory register value together and store it in the data register
    self.lines.append("D=D+M")

  def __sub(self):
    # Add the Data register and the memory register value together and store it in the data register
    self.lines.append("D=M-D")

  def _arithmetic(self, func):
    # Get first value
    self.stack.pop()
    # Store RAM memory register value in CPU D(ata) register
    self.lines.append("D=M")
    # Get second value
    self.stack.pop()
    # compute
    func()
    # push result back to memory and update stack pointer
    self.stack.push()

  def _neg(self):
    self.stack.get()
    # Write back negated value to the register directly
    # this way doing push in one step
    self.lines.append("M=-M")

  def _not(self):
    self.stack.get()
    # Write back not value to the register directly
    # this way doing push in one step
    self.lines.append("M=!M")
